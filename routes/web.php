<?php

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('bank-branches/fetch-banks', 'BankBranchesController@fetch_banks');
Route::get('accounts/fetch-bank-branches', 'AccountController@fetch_bank_branches');
Route::get('branches/fetch-company-branches', 'BranchController@fetch_company_branches');
Route::get('holiday-maintenance/fetch-holidays', 'HolidayCalendarController@fetch_holidays');

Route::get('cheques/queue', 'ChequesInQueueController@index')->name('cheques.queue.index');
Route::get('cheques/load-to-queue', 'ChequesInQueueController@load_to_queue');
Route::post('cheques/load-next-to-queue', 'ChequesInQueueController@load_next_to_queue');
Route::post('cheques/load-prev-to-queue', 'ChequesInQueueController@load_prev_to_queue');
Route::get('cheques/load-cheque-from-select', 'ChequesInQueueController@load_cheque_from_select');
Route::post('cheques/save-cheques-in-queue', 'ChequesInQueueController@save_cheques_in_queue');
Route::post('cheques/save-ciq-field-value', 'ChequesInQueueController@save_ciq_field_value');
Route::post('cheques/remove-cheque-from-queue', 'ChequesInQueueController@remove_cheque_from_queue');
Route::get('cheques/get-values-for-setting-queue-default-type', 'ChequesInQueueController@get_values_for_setting_queue_default_type');
Route::post('cheques/set-queue-default', 'ChequesInQueueController@set_queue_default');

Route::get('cheques/hand', 'ChequesInHandController@index')->name('cheques.hand.index');
Route::get('cheques/hand-hold', 'ChequesInHandController@on_hold_cheques')->name('cheques.hand-hold.index');
Route::post('cheques/select-cheques-in-hand', 'ChequesInHandController@select_cheques_in_hand');
Route::get('cheques/hold-cheque', 'ChequesInHandController@hold_cheque');
Route::get('cheques/release-cheque', 'ChequesInHandController@release_cheque');
Route::get('cheques/hold-selected-cheques', 'ChequesInHandController@hold_selected_cheques');
Route::get('cheques/release-selected-cheques', 'ChequesInHandController@release_selected_cheques');
Route::get('cheques/show-cheque-image', 'ChequesInHandController@show_cheque_image');

Route::get('cheques/grant-cheques', 'GrantChequesController@grant_cheques');
Route::post('cheques/load-selected-cheque-in-grant-queue', 'GrantChequesController@load_selected_cheque_in_grant_queue');
Route::post('cheques/submit-grant-cheques', 'GrantChequesController@submit_grant_cheques');

Route::get('cheques/banks', 'ChequesToBankController@index')->name('cheques.bank.index');
Route::get('cheques/bank-return', 'ChequesToBankController@return_cheques')->name('cheques.bank-return.index');
Route::get('cheques/mark-return', 'ChequesToBankController@mark_return');
Route::post('cheques/submit-return-cheques', 'ChequesToBankController@submit_return_cheques');

Route::get('cheques/vendors', 'ChequesToVendorController@index')->name('cheques.vendor.index');

Route::get('cheques/scanned', 'ScannedChequesController@index')->name('cheques.scanned.index');
Route::get('cheques/scanned/show-cheque-image', 'ScannedChequesController@show_cheque_image');

Route::resource('banks', 'BanksController');
Route::resource('bank-branches', 'BankBranchesController');
Route::resource('branches', 'BranchController');
Route::resource('return-reasons', 'ReturnReasonController');
Route::resource('vendors', 'VendorController');
Route::resource('accounts', 'AccountController');
Route::resource('holiday-maintenance', 'HolidayCalendarController');

Route::get('reports', 'ReportsController@index')->name('reports.index');
Route::post('reports/generate', 'ReportsController@generate')->name('reports.generate');
Route::get('reports/get-customer', 'ReportsController@get_customer');
Route::post('reports/validate-input', 'ReportsController@validate_input')->name('reports.input_validity');
Route::get('/customer/print-pdf', 'ReportsController@printpdf')->name('reports.printpdf');

Route::post('sys-date/update', 'SystemDateController@update')->name('sys-date.update');
Route::post('sys-date/undo', 'SystemDateController@undo')->name('sys-date.undo');
Route::post('home/open_scanner', 'HomeController@open_scanner')->name('open_scanner');
