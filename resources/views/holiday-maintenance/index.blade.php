@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset("app-assets/vendors/css/pickers/daterange/daterangepicker.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("app-assets/vendors/css/pickers/pickadate/pickadate.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("app-assets/css/plugins/pickers/daterange/daterange.css") }}">
<link href='{{ asset("app-assets/fullcalendar/core/main.css") }}' rel='stylesheet' />
<link href='{{ asset("app-assets/fullcalendar/daygrid/main.css") }}' rel='stylesheet' />

<style>
    .fc-title {
        color: #fff !important
    }
</style>

@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Holiday Maintenance</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add or Delete Holidays
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Holidays</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createHolidayEvent">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createHolidayEvent" role="dialog" aria-labelledby="createHolidayEventModal" aria-hidden="true">
										<div class="modal-dialog modal-md" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createHolidayEventModal">Add Holiday Event</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_holiday_form">
											  	  <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="title">Title </label>
                                                                    <input type="text" class="form-control holiday" id="title" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label>Date</label>
                                                                    <div class='input-group'>
                                                                        <input type='text' class="form-control showdropdowns"  id="daterange"/>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
												  </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_holiday_event" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>
  
  <div class="modal fade text-left" id="editHolidayEvent" role="dialog" aria-labelledby="editHolidayEventModal" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="editHolidayEventModal">Edit Holiday Event</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="add_holiday_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group form-group-style">
                                <label for="title">Title </label>
                                <input type="text" class="form-control holiday" id="edit_title" >
                                <input type="hidden" class="form-control holiday" id="edit_id" >
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group form-group-style">
                                <label>Date</label>
                                <div class='input-group'>
                                    <input type='text' class="form-control showdropdowns"  id="edit_daterange"/>
                                </div>
                            </fieldset>
                        </div>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="remove_holiday_event" class="btn btn-danger btn-glow" >Remove</button>
                <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Cancel</button>
                <button type="button" id="update_holiday_event" class="btn btn-success btn-glow"> Update </button>
              </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')

<script src="{{ asset("app-assets/vendors/js/pickers/pickadate/picker.js") }}"></script>
<script src="{{ asset("app-assets/vendors/js/pickers/pickadate/picker.date.js") }}"></script>
<script src="{{ asset("app-assets/vendors/js/pickers/pickadate/picker.time.js") }}"></script>
<script src="{{ asset("app-assets/vendors/js/pickers/pickadate/legacy.js") }}"></script>
<script src="{{ asset("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") }}"></script>
<script src="{{ asset("app-assets/vendors/js/pickers/daterange/daterangepicker.js") }}"></script>


<script src='{{ asset("app-assets/fullcalendar/core/main.js") }}'></script>
<script src='{{ asset("app-assets/fullcalendar/daygrid/main.js") }}'></script>

<script>
    $('.showdropdowns').daterangepicker({
        showDropdowns: true,
        autoApply: true,
		locale: {
			format: 'YYYY-MM-DD'
		}
    });

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var SITEURL = "{{url('holiday-maintenance/fetch-holidays')}}";

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ],
        events: SITEURL,
        eventClick: function(info) {
            $.ajax({
                type: "GET",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: 'holiday-maintenance/'+info.event.id+'/edit',
                dataType: "json",
                data: "id="+info.event.id,
                success: function(response) {
                    $('#edit_id').val(response.id);
                    $('#edit_title').val(response.title);

                    end = response.end;
                    end = end.split('-');
                    end = end[0] + '-' + end[1] + '-' + (parseInt(end[2]) - 1);

                    $('#edit_daterange').daterangepicker({
                        showDropdowns: true,
                        autoApply: true,
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        startDate: response.start,
                        endDate: end
                    });
                    $('#editHolidayEvent').modal('show');
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                }
            });
        }
      });

      calendar.render();

      
        $('#submit_holiday_event').on('click',function() {
            $('#submit_holiday_event').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
            $('#submit_holiday_event').attr('disabled', 'true');

            var data = {
                'title' : $("#title").val(),
                'daterange' : $("#daterange").val()
            };

            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ route("holiday-maintenance.store") }}',
                data: data,
                dataType: "json",
                success: function(response) {
                    if(response.type == 'success') {
                        toastr.success(response.text);

                        $('#submit_holiday_event').html('Save');
                        $('#submit_holiday_event').removeAttr('disabled');

                        $("#title").val('');
                        calendar.refetchEvents()
                        
                    } else if (response.type == 'error') {
                        toastr.error(response.text);
                        $('#submit_holiday_event').html('Save');
                        $('#submit_holiday_event').removeAttr('disabled');
                    } else {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('#submit_holiday_event').html('Save');
                        $('#submit_holiday_event').removeAttr('disabled');
                    }
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_holiday_event').html('Save');
                    $('#submit_holiday_event').removeAttr('disabled');
                }
            });

        });

        $('#update_holiday_event').on('click',function() {
            $('#update_holiday_event').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
            $('#update_holiday_event').attr('disabled', 'true');

            var data = {
                'id' : $("#edit_id").val(),
                'title' : $("#edit_title").val(),
                'daterange' : $("#edit_daterange").val()
            };

            $.ajax({
                type: "PUT",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: 'holiday-maintenance/'+$("#edit_id").val(),
                data: data,
                dataType: "json",
                success: function(response) {
                    if(response.type == 'success') {
                        toastr.success(response.text);

                        $('#update_holiday_event').html('Update');
                        $('#update_holiday_event').removeAttr('disabled');

                        calendar.refetchEvents()
                        
                    } else if (response.type == 'error') {
                        toastr.error(response.text);
                        $('#update_holiday_event').html('Update');
                        $('#update_holiday_event').removeAttr('disabled');
                    } else {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('#update_holiday_event').html('Update');
                        $('#update_holiday_event').removeAttr('disabled');
                    }
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#update_holiday_event').html('Update');
                    $('#update_holiday_event').removeAttr('disabled');
                }
            });

        });

        $('#remove_holiday_event').on('click',function() {
            $('#remove_holiday_event').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
            $('#remove_holiday_event').attr('disabled', 'true');

            var data = {
                'id' : $("#edit_id").val()
            };

            $.ajax({
                type: "DELETE",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: 'holiday-maintenance/'+ $("#edit_id").val(),
                data: data,
                dataType: "json",
                success: function(response) {
                    if(response.type == 'success') {
                        toastr.success(response.text);

                        $('#remove_holiday_event').html('Remove');
                        $('#remove_holiday_event').removeAttr('disabled');
                        $('#editHolidayEvent').modal('hide');
                        calendar.refetchEvents()
                        
                    } else if (response.type == 'error') {
                        toastr.error(response.text);
                        $('#remove_holiday_event').html('Remove');
                        $('#remove_holiday_event').removeAttr('disabled');
                    } else {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('#remove_holiday_event').html('Remove');
                        $('#remove_holiday_event').removeAttr('disabled');
                    }
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#remove_holiday_event').html('Remove');
                    $('#remove_holiday_event').removeAttr('disabled');
                }
            });

        });
    });

</script>
@endsection
