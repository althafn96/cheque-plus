@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<style>
    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
</style>
@endsection

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Reports</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">generate management reports
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form id="report_parameters_form" class="row">
                                        <div class="col-md-4">
                                            <fieldset class="form-group form-group-style">
                                                <label for="type">Type <span class="red">*</span></label>
                                                <select name="type" id="type" class="select2 form-control">
                                                    <option value="0">-- SELECT TYPE --</option>
                                                    <option value="1">Received Cheque Details</option>
                                                    <option value="3">Cheques in Hand Details (Payee Wise)</option>
                                                    <option value="7">Cheques in Hand Summary (Payee Wise)</option>
                                                    <option value="6">Cheques in Hand Details (Customer Wise)</option>
                                                    <option value="8">Cheques in Hand Summary (Customer Wise)</option>
                                                    <option value="4">Customer Individual Report</option>
                                                    <option value="2">Cheques Granted to Bank</option>
                                                    <option value="10">Cheque Deposit Details</option>
                                                    <option value="5">Cheques on Hold</option>
                                                    <option value="9">Assigned Cheques</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="form-group form-group-style">
                                                <label for="from_date">From <span class="red">*</span></label>
                                                <input value="{{ $sys_date }}" name="from_date" type="date" class="form-control" id="from_date">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="form-group form-group-style">
                                                <label for="to_date">To <span class="red">*</span></label>
                                                <input value="{{ $sys_date }}" name="to_date" type="date" class="form-control" id="to_date">
                                            </fieldset>
                                        </div>
                                        <div id="customer_select" style="display: none" class="col-md-4">
                                            <fieldset class="form-group form-group-style">
                                                <label for="customer">Customer <span class="red">*</span></label>
                                                <select name="customer" id="customer" class="select2 form-control">
                                                    <option value="0">-- SELECT CUSTOMER --</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <button style="" class="btn btn-info btn-min-width btn-cons" id="generate_report" type="submit" data-formname="purchase_order_request_add" data-posttype="POST">
                                                    <i class="la la-cog"></i> Generate
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    $('#type').select2();
    $('#customer').select2({
        ajax: {
            url: 'reports/get-customer',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type:  'public'
                }

                return query;
            },
            
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        },
        width: '100%'
    });

    $('#type').change(function() {
        if($('#type').val() == '4' || $('#type').val() == '9') {
            $('#customer_select').removeAttr('style');
        } else {
            $('#customer_select').css('display','none');
        }
    });

    $(document).ready(function() {
        $('#report_content_section').css('display', 'none');
    });

    $('#report_parameters_form').submit(function(e) {
        e.preventDefault();
        $('#generate_report').html('<i class="la la-gear spinner"></i> Loading...');
        $('#generate_report').attr('disabled', 'true');

        fdata = new FormData($("#report_parameters_form")[0]);

        $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("reports.input_validity") }}',
            processData: false,
            contentType: false,
            data:fdata,
            dataType: 'json',
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success('report will be downloaded shortly');
                    $.ajax({
                        type: "POST",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: '{{ route("reports.generate") }}',
                        processData: false,
                        contentType: false,
                        data:fdata,
                        xhrFields: {
                            responseType: 'blob'
                        },
                        success: function (response, status, xhr) {
                            console.log(response);

                            var filename = "";                   
                            var disposition = xhr.getResponseHeader('Content-Disposition');

                            if (disposition) {
                                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                var matches = filenameRegex.exec(disposition);
                                if (matches !== null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            } 
                            var linkelem = document.createElement('a');
                            try {
                                var blob = new Blob([response], { type: 'application/octet-stream' });                        

                                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                                    //   IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                                    window.navigator.msSaveBlob(blob, filename);
                                } else {
                                    var URL = window.URL || window.webkitURL;
                                    var downloadUrl = URL.createObjectURL(blob);

                                    if (filename) { 
                                        // use HTML5 a[download] attribute to specify filename
                                        var a = document.createElement("a");

                                        // safari doesn't support this yet
                                        if (typeof a.download === 'undefined') {
                                            window.location = downloadUrl;
                                        } else {
                                            a.href = downloadUrl;
                                            a.download = filename;
                                            document.body.appendChild(a);
                                            a.target = "_blank";
                                            a.click();
                                        }
                                    } else {
                                        window.location = downloadUrl;
                                    }
                                }   

                            } catch (ex) {
                                console.log(ex);
                            } 
                        },
                        error: function(response) {
                            toastr.warning('unknown error occurred. please contact management');
                        }
                    });
                } else {
                    toastr.error(response.text);
                }
                            
                $('#generate_report').html('<i class="la la-cog"></i> Generate');
                $('#generate_report').removeAttr('disabled');
            },
            error: function(response) {
                toastr.warning('unknown error occurred. please contact management');
                $('#generate_report').html('<i class="la la-cog"></i> Generate');
                $('#generate_report').removeAttr('disabled');
            }

        });

        
        
    });
</script>
@endsection
