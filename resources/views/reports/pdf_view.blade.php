<!DOCTYPE>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <style>
        @page { margin: 100px 0.75in; }
        header { position: fixed; top: -60px; left: 0px; right: 0px; height: 50px; border-bottom: 1px solid #8f9199 }
        footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 40px; border-top: 1px solid #8f9199; border-bottom: 1px solid #8f9199 }
        
        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: transparent;
        }
        p { page-break-after: always; }
        p:last-child { page-break-after: never; }
        table { font-size: 10px }
        article,
        aside,
        dialog,
        figcaption,
        figure,
        footer,
        header,
        hgroup,
        main,
        nav,
        section {
            display: block;
        }
        .nav-tabs .nav-item {
            margin-bottom: -1px;
        }
        .nav-tabs .nav-item.show .nav-link,
        .nav-tabs .nav-link.active {
            color: #495057;
            background-color: #fff;
            border-color: #dee2e6 #dee2e6 #fff;
        }
        .nav-fill .nav-item {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            text-align: center;
        }
        .nav-justified .nav-item {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            text-align: center;
        }
        .navbar-brand {
            display: inline-block;
            padding-top: 0.3125rem;
            padding-bottom: 0.3125rem;
            margin-right: 1rem;
            font-size: 1.25rem;
            line-height: inherit;
            white-space: nowrap;
        }
        .navbar-brand:focus,
        .navbar-brand:hover {
            text-decoration: none;
        }
        .navbar-nav {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .navbar-nav .nav-link {
            padding-right: 0;
            padding-left: 0;
        }
        .navbar-nav .dropdown-menu {
            position: static;
            float: none;
        }
        .navbar-text {
            display: inline-block;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }
        .navbar-light .navbar-brand {
            color: rgba(0, 0, 0, 0.9);
        }
        .navbar-dark .navbar-brand {
            color: #fff;
        }
        .navbar-brand {
            display: inline-block;
            padding-top: 0.3125rem;
            padding-bottom: 0.3125rem;
            margin-right: 1rem;
            font-size: 1.25rem;
            line-height: inherit;
            white-space: nowrap;
        }
        .flex-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }
        .mr-auto,
        .mx-auto {
            margin-right: auto !important;
        }
        .float-right {
            float: right !important;
        }
        table {
            border-collapse: collapse;
        }.table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }
        .table td,
        .table th {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table tbody + tbody {
            border-top: 2px solid #dee2e6;
        }
        .table .table {
            background-color: #fff;
        }
        .position-relative {
            position: relative !important;
        }
    </style>
    <title>{{ $title }}</title>
</head>
<body>
    
    <script type="text/php">
        if (isset($pdf)) {
            $x = 275;
            $y = 790;
            $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
            $font = null;
            $size = 7;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
    </script>
    <header >
        <nav class="header-navbar">
            <div class="navbar-wrapper">
              <div class="navbar-header expanded">
                <ul class="nav navbar-nav flex-row position-relative">
                  <li class="nav-item mr-auto">
                      <a class="navbar-brand" href="#">
                          <img style="width: 16%; margin-top: -15px" class="brand-logo" src="{{ asset('app-assets/images/asian-logo.png') }}">
                      </a>
                </ul>
              </div>
              <div class="navbar-container content">
                <div class="" id="navbar-mobile">
                  <ul class="nav navbar-nav float-right">
                    <li>{{ $heading }}</li>
                  </ul>
                </div>
              </div>
            </div>
        </nav>
    </header>
    <footer>
        <div>
            <img style="margin-top: 6px" src="{{ asset('app-assets/images/pragicts-logo.png') }}" alt="">
        </div>
        <div>
            <span style="font-size: 10px; margin-top: -20px" class="float-right">Generated On: {{ \Carbon\Carbon::now()->setTimezone('Asia/Colombo') }}</span>
        </div>
    </footer>
    <div>
        <table style="margin-bottom: 10px">
            <tr>
                <td style="width: 10rem">From: <b>{{ $report_data['from'] }}</b></td>
                <td style="width: 10rem; text-align: left">To: <b>{{ $report_data['to'] }}</b></td>
                @if($r_type == '4' || $r_type == '9')
                <td style="width: 10rem; text-align: left">Customer: <b>{{ $report_data['customer'] }}</b></td>
                <td style="width: 10rem; text-align: right">System Date: <b>{{ $sys_date }}</b></td>
                @else
                <td style="width: 20rem; text-align: right">System Date: <b>{{ $sys_date }}</b></td>
                @endif
            </tr>
        </table>
        @php 
            $t_i = 0; 
            $t_amount = 0; 
        @endphp
        @if($r_type == '1' || $r_type == '3' || $r_type == '2' || $r_type == '4' || $r_type == '5')
            @foreach($grouping_set as $payee)
                <h6 style="margin-bottom: 0">{{ $payee->payee_name }}</h6>
                <table style="border: 1px solid #8f9199" class="table">
                    <thead>
                        <tr>
                            <th style="border: 1px solid #000">Chq No</th>
                            @if($r_type == '1' || $r_type == '4')
                                <th style="border: 1px solid #000">Received Date</th>
                                <th style="border: 1px solid #000">Deposit Date</th>
                            @elseif($r_type == '2')
                                <th style="border: 1px solid #000">Account No</th>
                                <th style="border: 1px solid #000">Granted Date</th>
                                <th style="border: 1px solid #000">Our Account No</th>
                            @elseif($r_type == '3')
                                <th style="border: 1px solid #000">Deposit Date</th>
                                <th style="border: 1px solid #000">Hold</th>
                            @elseif($r_type == '5')
                                <th style="border: 1px solid #000">Deposit Date</th>
                            @endif
                            <th style="border: 1px solid #000">Bank Code / Name</th>
                            <th style="border: 1px solid #000">Branch Code / Name</th>
                            <th style="border: 1px solid #000">Amount (Rs)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                            $i = 0; 
                            $amount = 0; 
                        @endphp
                                @foreach($cheques as $chq)
                                    @if($payee->payee_id == $chq->payee_id)
                                        <tr>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->cheque_no }}</td>
                                            @if($r_type == '1' || $r_type == '4')
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->received_date }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                                            @elseif($r_type == '2')
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->account_no }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->granted_date }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->our_account_no }}</td>
                                            @elseif($r_type == '3')
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: center">{{ $chq->is_hold }}</td>
                                            @elseif($r_type == '5')
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                                            @endif
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bank_code }} / {{ $chq->bank_name }}</td>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bb_code }} / {{ $chq->bb_name }}</td>
                                            <td style="text-align: right;border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->amount }}</td>
                                        </tr>
                                        @php
                                            $i++;
                                            $t_i++;
                                            $amount += $chq->amount;
                                            $t_amount += $chq->amount;
                                        @endphp
                                    @endif
                                @endforeach
                            <tr>
                                <td></td>
                                @if($r_type == '1' || $r_type == '3' || $r_type == '4')
                                <td></td>
                                <td></td>
                                @elseif($r_type == '2')
                                <td></td>
                                <td></td>
                                <td></td>
                                @elseif($r_type == '5')
                                <td></td>
                                @endif
                                <td> <b>Cheques / Amount</b> </td>
                                <td><b>{{$i}}</b></td>
                                <td style="text-align: right"><b>{{$amount}}</b></td>
                            </tr>
                    </tbody>
                </table>
                
            @endforeach
        @elseif($r_type == '6')
            @foreach($grouping_set as $vendor)
                <table>
                    <tr>
                        <td style="width: 10rem">Vendor Code: {{ $vendor->vendor_code }}</b></td>
                        <td style="width: 10rem">Vendor Name: {{ $vendor->vendor_name }}</b></td>
                    </tr>
                </table>
                <table style="border: 1px solid #8f9199" class="table">
                    <thead>
                        <tr>
                            <th style="border: 1px solid #000">Chq No</th>
                            <th style="border: 1px solid #000">Deposit Date</th>
                            <th style="border: 1px solid #000">Bank Code / Name</th>
                            <th style="border: 1px solid #000">Branch Code / Name</th>
                            <th style="border: 1px solid #000">Amount (Rs)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                            $i = 0; 
                            $amount = 0; 
                        @endphp
                                @foreach($cheques as $chq)
                                    @if($vendor->vendor_id == $chq->vendor_id)
                                        <tr>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->cheque_no }}</td>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bank_code }} / {{ $chq->bank_name }}</td>
                                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bb_code }} / {{ $chq->bb_name }}</td>
                                            <td style="text-align: right;border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->amount }}</td>
                                        </tr>
                                        @php
                                            $i++;
                                            $t_i++;
                                            $amount += $chq->amount;
                                            $t_amount += $chq->amount;
                                        @endphp
                                    @endif
                                @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td> <b>Cheques / Amount</b> </td>
                                <td><b>{{$i}}</b></td>
                                <td style="text-align: right"><b>{{$amount}}</b></td>
                            </tr>
                    </tbody>
                </table>
                
            @endforeach 
        @elseif($r_type == '7')
                @foreach($grouping_set as $payee)
                    <h6 style="margin-bottom: 0">{{ $payee->payee_name }}</h6>
                    <table style="border: 1px solid #8f9199" class="table">
                        <thead>
                            <tr>
                                <th style="border: 1px solid #000">Deposit Date</th>
                                <th style="border: 1px solid #000">No. of Cheques</th>
                                <th style="border: 1px solid #000">Cheque Value (Rs)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php 
                                $i = 0; 
                                $amount = 0; 
                            @endphp
                                    @foreach($cheques as $chq)
                                        @if($payee->payee_id == $chq->payee_id)
                                            <tr>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: center">{{ $chq->count }}</td>
                                                <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: right">{{ $chq->sum }}</td>
                                            </tr>
                                            @php
                                                $i += $chq->count;
                                                $t_i += $chq->count;
                                                $amount += $chq->sum;
                                                $t_amount += $chq->sum;
                                            @endphp
                                        @endif
                                    @endforeach
                                <tr>
                                    @if($r_type == '7')
                                    <td><b>Cheques / Amount</b> </td>
                                    <td style="text-align: center"><b>{{$i}}</b></td>
                                    <td style="text-align: right"><b>{{$amount}}</b></td>
                                    @else
                                    <td></td>
                                    <td></td>
                                    <td> <b>Cheques / Amount</b> </td>
                                    <td style="text-align: center"><b>{{$i}}</b></td>
                                    <td style="text-align: right"><b>{{$amount}}</b></td>
                                    @endif
                                </tr>
                        </tbody>
                    </table>
                    
                @endforeach 
        @elseif($r_type == '8')
            <table style="border: 1px solid #8f9199" class="table">
                <thead>
                    <tr>
                        <th style="border: 1px solid #000">Customer Code</th>
                        <th style="border: 1px solid #000">Customer Name</th>
                        <th style="border: 1px solid #000">No. of Cheques</th>
                        <th style="border: 1px solid #000">Cheque value (Rs)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cheques as $chq)
                        <tr>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: center">{{ $chq->vendor_code }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->vendor_name }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: center">{{ $chq->count }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: right">{{ $chq->sum_value }}</td>
                        </tr>
                        @php
                            $t_i += $chq->count;
                            $t_amount += $chq->sum_value;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        @elseif($r_type == '9')
            <table style="border: 1px solid #8f9199" class="table">
                <thead>
                    <tr>
                        <th style="border: 1px solid #000">Cheque No</th>
                        <th style="border: 1px solid #000">Bank Code / Name</th>
                        <th style="border: 1px solid #000">Branch Code / Name</th>
                        <th style="border: 1px solid #000">Deposit Date</th>
                        <th style="border: 1px solid #000">Granted Date</th>
                        <th style="border: 1px solid #000">Amount (Rs)</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $i = 0; 
                        $amount = 0; 
                    @endphp
                    @foreach($cheques as $chq)
                        <tr>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px; text-align: center">{{ $chq->cheque_no }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bank_code }} / {{ $chq->bank_name }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->bb_code }} / {{ $chq->bb_name }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->deposit_date }}</td>
                            <td style="border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->granted_date }}</td>
                            <td style="text-align: right;border: 1px solid #8f9199; padding-top: 6px; padding-bottom: 6px">{{ $chq->amount }}</td>
                        </tr>
                        @php
                            $i++;
                            $t_i++;
                            $amount += $chq->amount;
                            $t_amount += $chq->amount;
                        @endphp
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td> <b>Cheques / Amount</b> </td>
                        <td style="text-align: center"><b>{{$i}}</b></td>
                        <td style="text-align: right"><b>{{$amount}}</b></td>
                    </tr>
                </tbody>
            </table>
        @endif
        <table class="table">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td> <b>Total Cheques / Total Amount</b> </td>
                <td><b>{{$t_i}}</b></td>
                <td style="text-align: right"><b>{{$t_amount}}</b></td>
            </tr>
        </table>
    </div>
</body>
</body>
</html>