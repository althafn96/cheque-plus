@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">


@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Branches</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add, Update or Delete Branches
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Branches</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createBranch">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createBranch" role="dialog" aria-labelledby="createBranchModal" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createBranchModal">Create Branch</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_bank_form">
											  	  <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="brnch_name">Branch Name <span class="red">*</span></label>
                                                                    <input type="text" class="form-control brnch_field" id="brnch_name" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="brnch_location">Location</label>
                                                                    <input type="text" class="form-control brnch_field" id="brnch_location" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="brnch_contact_name">Contact Name <span class="red">*</span></label>
                                                                    <input type="text" class="form-control brnch_field" id="brnch_contact_name" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="brnch_contact_no">Contact Number</label>
                                                                    <input type="text" class="form-control brnch_field" id="brnch_contact_no" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="brnch_email">Email</label>
                                                                    <input type="email" class="form-control brnch_field" id="brnch_email" >
                                                                </fieldset>
                                                            </div>
                                                        </div>
												  </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_branch" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="branches_list">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Contact Name</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>

    <div class="modal fade text-left" id="editBranch" role="dialog" aria-labelledby="editBranchModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editBranchModal">Edit Bank Branch</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="edit_bank_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_brnch_name">Branch Name <span class="red">*</span></label>
                                    <input type="text" class="form-control edit_brnch_field" id="edit_brnch_name" >
                                    <input type="hidden" class="form-control edit_brnch_field" id="edit_id" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_brnch_location">Location</label>
                                    <input type="text" class="form-control edit_brnch_field" id="edit_brnch_location" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_brnch_contact_name">Contact Name <span class="red">*</span></label>
                                    <input type="text" class="form-control edit_brnch_field" id="edit_brnch_contact_name" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_brnch_contact_no">Contact Number</label>
                                    <input type="text" class="form-control edit_brnch_field" id="edit_brnch_contact_no" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_brnch_email">Email</label>
                                    <input type="email" class="form-control edit_brnch_field" id="edit_brnch_email" >
                                </fieldset>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_branch" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script>
    var table = $('#branches_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('branches.index') }}"
        },
        columns: [
            {
                data: 'brnch_name',
                name: 'brnch_name'
            },
            {
                data: 'brnch_location',
                name: 'brnch_location',
                visible: false
            },
            {
                data: 'brnch_contact_name',
                name: 'brnch_contact_name'
            },
            {
                data: 'brnch_contact_no',
                name: 'brnch_contact_no'
            },
            {
                data: 'brnch_email',
                name: 'brnch_email'
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.status == '1') {
                        return '<div class="badge badge-success">Enabled</div>';
                    } else {
                        return '<div class="badge badge-danger">Disabled</div>';
                    }
                    
                },
                name: 'status'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_branch').on('click',function() {
        $('#submit_branch').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_branch').attr('disabled', 'true');

        var data = {
            'brnch_name' : $("#brnch_name").val(),
            'brnch_location' : $("#brnch_location").val(),
            'brnch_contact_name' : $("#brnch_contact_name").val(),
            'brnch_contact_no' : $("#brnch_contact_no").val(),
            'brnch_email' : $("#brnch_email").val()
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("branches.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_branch').html('Save');
                    $('#submit_branch').removeAttr('disabled');

                    $("#brnch_name").val('');
                    $("#brnch_location").val('');
                    $("#brnch_contact_name").val('');
                    $("#brnch_contact_no").val('');
                    $("#brnch_email").val('');

                    setTimeout(function(){ 
                        $('#createBranch').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_branch').html('Save');
                    $('#submit_branch').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_branch').html('Save');
                    $('#submit_branch').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_branch').html('Save');
                $('#submit_branch').removeAttr('disabled');
            }
        });

    });

    $('#update_branch').on('click',function() {
        $('#update_branch').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_branch').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'brnch_name' : $("#edit_brnch_name").val(),
            'brnch_location' : $("#edit_brnch_location").val(),
            'brnch_contact_name' : $("#edit_brnch_contact_name").val(),
            'brnch_contact_no' : $("#edit_brnch_contact_no").val(),
            'brnch_email' : $("#edit_brnch_email").val()
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'branches/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_branch').html('Update');
                $('#update_branch').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_branch').html('Update');
                $('#update_branch').removeAttr('disabled');
            }
        });

    });


    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'branches/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.id);
                $("#edit_brnch_name").val(response.brnch_name);
                $("#edit_brnch_location").val(response.brnch_location);
                $("#edit_brnch_contact_name").val(response.brnch_contact_name);
                $("#edit_brnch_contact_no").val(response.brnch_contact_no);
                $("#edit_brnch_email").val(response.brnch_email);

                $('#editBranch').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'branches/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

</script>
@endsection
