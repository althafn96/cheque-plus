<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">

      <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item {{ $pageTitle == 'Home' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('home') }}"><i class="la la-dashboard"></i><span class="menu-title">Dashboard</span></a>
          </li>
          <li data-menu="dropdown" class="dropdown nav-item 
          {{ $pageTitle == 'Cheques in Queue' || 
          $pageTitle == 'Cheques in Hand' || 
          $pageTitle == 'Cheques to Bank' || 
          $pageTitle == 'Cheques to Vendor' ? 'active' : '' 
          }}"><a class="dropdown-toggle nav-link" href="#"><i class="la la-money"></i><span class="menu-title">Cheques</span></a>
            <ul class="dropdown-menu">
              <li class="{{ $pageTitle == 'Cheques in Queue' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('cheques.queue.index') }}">Cheques in Queue</a>
              </li>
              <li class="{{ $pageTitle == 'Cheques in Hand' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('cheques.hand.index') }}">Cheques in Hand</a>
              </li>
              <li class="{{ $pageTitle == 'Cheques to Bank' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('cheques.bank.index') }}">Cheques to Bank</a>
              </li>
              <li class="{{ $pageTitle == 'Cheques to Vendor' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('cheques.vendor.index') }}">Cheques to Vendor</a>
              </li>
              <li class="{{ $pageTitle == 'Scanned Cheques' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('cheques.scanned.index') }}">Scanned Cheques</a>
              </li>
            </ul>
          </li>
          <li data-menu="dropdown" class="dropdown nav-item
          {{ $pageTitle == 'Banks' || $pageTitle == 'Bank Branches' ? 'active' : '' }}">
          <a class="dropdown-toggle nav-link" href="#"><i class="la la-bank"></i><span class="menu-title">Banks</span></a>
            <ul class="dropdown-menu">
              <li class="{{ $pageTitle == 'Banks' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('banks.index') }}">Banks</a>
              </li>
              <li class="{{ $pageTitle == 'Bank Branches' ? 'active' : ''}}"><a class="dropdown-item" href="{{ route('bank-branches.index') }}">Banks Branches</a>
              </li>
            </ul>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Branches' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('branches.index') }}"><i class="la la-sitemap"></i><span class="menu-title">Branches</span></a>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Accounts' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('accounts.index') }}"><i class="la la-folder-open"></i><span class="menu-title">Accounts</span></a>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Return Reasons' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('return-reasons.index') }}"><i class="la la-step-backward"></i><span class="menu-title">Return Reasons</span></a>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Holiday Maintenance' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('holiday-maintenance.index') }}"><i class="la la-calendar"></i><span class="menu-title">Holiday Maintenance</span></a>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Vendors' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('vendors.index') }}"><i class="la la-group"></i><span class="menu-title">Vendors</span></a>
          </li>
          <li class=" nav-item {{ $pageTitle == 'Reports' ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('reports.index') }}"><i class="la la-print"></i><span class="menu-title">Reports</span></a>
          </li>
        </ul>
      </div>
</div>