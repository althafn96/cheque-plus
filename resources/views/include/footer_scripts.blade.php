<!-- BEGIN VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/chart.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/raphael-min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/morris.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}"></script>
    <script src="{{ asset('app-assets/data/jvector/visitor-data.js') }}"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <!-- END MODERN JS-->

    <script>
        $('#eod_update').click(function () {
            
            $('#eod_update').html('<i class="la la-circle-o-notch spinner"></i> Ending Day Process...');
            $('#eod_update').attr('disabled', 'true');

            var data = {
                'sys_date' : $("#sys_date").val(),
            };

            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ route("sys-date.update") }}',
                data: data,
                dataType: "json",
                success: function(response) {
                    if(response.type == 'success') {
                        toastr.success(response.text);

                        $('#eod_update').html('End Day Process');
                        $('#eod_update').removeAttr('disabled');

                        $("#sys_date").val(response.sys_date);

                        setTimeout(function(){ 
                            location.reload();
                        }, 1000);
                        
                    } else if (response.type == 'error') {
                        toastr.error(response.text);
                        $('#eod_update').html('End Day Process');
                        $('#eod_update').removeAttr('disabled');
                    } else {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('#eod_update').html('End Day Process');
                        $('#eod_update').removeAttr('disabled');
                    }
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#eod_update').html('End Day Process');
                    $('#eod_update').removeAttr('disabled');
                }
            });
        });

        $('#open_scanner').click(function() {

            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ route("open_scanner") }}',
                dataType: "json",
                success: function(response) {
                }
            });
        });

        
        $('#eod_undo').click(function () {
            
            $('#eod_undo').html('<i class="la la-circle-o-notch spinner"></i> Undoing End Day Process...');
            $('#eod_undo').attr('disabled', 'true');

            var data = {
                'sys_date' : $("#sys_date").val(),
            };

            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ route("sys-date.undo") }}',
                data: data,
                dataType: "json",
                success: function(response) {
                    if(response.type == 'success') {
                        toastr.success(response.text);

                        $('#eod_undo').html('Undo');
                        $('#eod_undo').removeAttr('disabled');

                        $("#sys_date").val(response.sys_date);
                        setTimeout(function(){ 
                            location.reload();
                        }, 1000);
                        
                    } else if (response.type == 'error') {
                        toastr.error(response.text);
                        $('#eod_undo').html('Undo');
                        $('#eod_undo').removeAttr('disabled');
                    } else {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('#eod_undo').html('Undo');
                        $('#eod_undo').removeAttr('disabled');
                    }
                },
                error:  function(response) {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#eod_undo').html('Undo');
                    $('#eod_undo').removeAttr('disabled');
                }
            });
        });
    </script>