<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="PragICTS">
    <title>{{ $pageTitle }} | {{ config('app.name') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    @include('include.header_styles')
    @yield('page_css')
  </head>
  <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    @include('include.topbar')


@include('include.sidenav')
    

   @yield('content')

    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; {{ date('Y') }}, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Engineered by <a class="text-bold-800 grey darken-2" href="http://pragicts.com/" style="color: #f71735" target="_blank">PragICTS </a></span></p>
    </footer>
    @include('include.footer_scripts')
    @yield('page_js')
  </body>
</html>