@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Banks</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add, Update or Delete Banks
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Banks</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createBank">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createBank" tabindex="-1" role="dialog" aria-labelledby="createBankModal" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createBankModal">Create Bank</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_bank_form">
											  	  <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="code">Bank Code <span class="red">*</span></label>
                                                                <input type="text" class="form-control bank_field" id="code" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="name">Bank Name <span class="red">*</span></label>
                                                                <input type="text" class="form-control bank_field" id="name" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="shortname">Bank Short Name <span class="red">*</span></label>
                                                                <input type="text" class="form-control bank_field" id="shortname" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_address_1">Address 1 <span class="red">*</span></label>
                                                                <input type="text" class="form-control bank_field" id="ho_address_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_address_2">Address 2</label>
                                                                <input type="text" class="form-control bank_field" id="ho_address_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_address_3">Address 3</label>
                                                                <input type="text" class="form-control bank_field" id="ho_address_3" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_tel_1">Telephone No (1) <span class="red">*</span></label>
                                                                <input type="text" class="form-control bank_field" id="ho_tel_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_tel_2">Telephone No (2)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_tel_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_tel_3">Telephone No (3)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_tel_3" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_fax_1">Fax No (1)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_fax_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_fax_2">Fax No (2)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_fax_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_email_1">Email (1)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_email_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="ho_email_2">Email (2)</label>
                                                                <input type="text" class="form-control bank_field" id="ho_email_2" >
                                                            </fieldset>
                                                        </div>
                                                    </div>
												  </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_bank" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="banks_list">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Bank</th>
                                                <th>Head Office Address</th>
                                                <th>Head Office Telephone</th>
                                                <th>Head Office Email</th>
                                                <th>Head Office Fax</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>

    <div class="modal fade text-left" id="editBank" tabindex="-1" role="dialog" aria-labelledby="editBankModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editBankModal">Edit Bank</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="add_bank_form">
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_code">Bank Code <span class="red">*</span></label>
                                <input type="text" class="form-control bank_field" id="edit_code" >
                                <input type="hidden" class="form-control bank_field" id="edit_id" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_name">Bank Name <span class="red">*</span></label>
                                <input type="text" class="form-control bank_field" id="edit_name" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_shortname">Bank Short Name <span class="red">*</span></label>
                                <input type="text" class="form-control bank_field" id="edit_shortname" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_address_1">Address 1 <span class="red">*</span></label>
                                <input type="text" class="form-control bank_field" id="edit_ho_address_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_address_2">Address 2</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_address_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_address_3">Address 3</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_address_3" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_tel_1">Telephone No (1) <span class="red">*</span></label>
                                <input type="text" class="form-control bank_field" id="edit_ho_tel_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_tel_2">Telephone No (2)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_tel_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_tel_3">Telephone No (3)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_tel_3" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_fax_1">Fax No (1)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_fax_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_fax_2">Fax No (2)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_fax_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_email_1">Email (1)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_email_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_ho_email_2">Email (2)</label>
                                <input type="text" class="form-control bank_field" id="edit_ho_email_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_status">Status</label>
                                <select id="edit_status" class="select2 form-control">
                                    <option value="0">Disable</option>
                                    <option value="1">Enable</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_bank" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    var table = $('#banks_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('banks.index') }}"
        },
        columns: [
            {
                data: 'code',
                name: 'code'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.ho_address_1 == 'null') {
                        data.ho_address_1 = '';
                    }
                    if(data.ho_address_2 == 'null') {
                        data.ho_address_2 = '';
                    }
                    if(data.ho_address_3 == 'null') {
                        data.ho_address_3 = '';
                    }
                    return data.ho_address_1 + "<br/>" + data.ho_address_2 + "<br/>" + data.ho_address_3;
                },
                name: 'address',
                visible: false
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.ho_tel_1 == null) {
                        data.ho_tel_1 = '';
                    }
                    if(data.ho_tel_2 == null) {
                        data.ho_tel_2 = '';
                    }
                    if(data.ho_tel_3 == null) {
                        data.ho_tel_3 = '';
                    }
                    return "<a href='tel:"+ data.ho_tel_1 +"'>" + data.ho_tel_1 + "</a><br/><a href='tel:"+ data.ho_tel_2 +"'>" + data.ho_tel_2 + "</a><br/><a href='tel:"+ data.ho_tel_3 +"'>" + data.ho_tel_3;
                },
                name: 'telephone'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.ho_email_1 == null) {
                        data.ho_email_1 = '';
                    }
                    if(data.ho_email_2 == null) {
                        data.ho_email_2 = '';
                    }
                    
                    return "<a href='mailto:"+ data.ho_email_1 +"'>" + data.ho_email_1 + "</a><br/><a href='mailto:"+ data.ho_email_2 +"'>" + data.ho_email_2;
                },
                name: 'email'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.ho_fax_1 == null) {
                        data.ho_fax_1 = '';
                    }
                    if(data.ho_fax_2 == null) {
                        data.ho_fax_2 = '';
                    }
                    
                    return data.ho_fax_1 + "<br/>"+ data.ho_fax_2;
                },
                name: 'fax',
                visible: false
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.status == '1') {
                        return '<div class="badge badge-success">Enabled</div>';
                    } else {
                        return '<div class="badge badge-danger">Disabled</div>';
                    }
                    
                },
                name: 'status'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_bank').on('click',function() {
        $('#submit_bank').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_bank').attr('disabled', 'true');

        var data = {
            'code' : $("#code").val(),
            'name' : $("#name").val(),
            'shortname' : $("#shortname").val(),
            'ho_address_1' : $("#ho_address_1").val(),
            'ho_address_2' : $("#ho_address_2").val(),
            'ho_address_3' : $("#ho_address_3").val(),
            'ho_tel_1' : $("#ho_tel_1").val(),
            'ho_tel_2' : $("#ho_tel_2").val(),
            'ho_tel_3' : $("#ho_tel_3").val(),
            'ho_fax_1' : $("#ho_fax_1").val(),
            'ho_fax_2' : $("#ho_fax_2").val(),
            'ho_email_1' : $("#ho_email_1").val(),
            'ho_email_2' : $("#ho_email_2").val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("banks.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_bank').html('Save');
                    $('#submit_bank').removeAttr('disabled');

                    $("#code").val('');
                    $("#name").val('');
                    $("#shortname").val('');
                    $("#ho_address_1").val('');
                    $("#ho_address_2").val('');
                    $("#ho_address_3").val('');
                    $("#ho_tel_1").val('');
                    $("#ho_tel_2").val('');
                    $("#ho_tel_3").val('');
                    $("#ho_fax_1").val('');
                    $("#ho_fax_2").val('');
                    $("#ho_email_1").val('');
                    $("#ho_email_2").val('');

                    setTimeout(function(){ 
                        $('#createBank').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_bank').html('Save');
                    $('#submit_bank').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_bank').html('Save');
                    $('#submit_bank').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });

    });

    $('#update_bank').on('click',function() {
        $('#update_bank').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_bank').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'code' : $("#edit_code").val(),
            'name' : $("#edit_name").val(),
            'shortname' : $("#edit_shortname").val(),
            'ho_address_1' : $("#edit_ho_address_1").val(),
            'ho_address_2' : $("#edit_ho_address_2").val(),
            'ho_address_3' : $("#edit_ho_address_3").val(),
            'ho_tel_1' : $("#edit_ho_tel_1").val(),
            'ho_tel_2' : $("#edit_ho_tel_2").val(),
            'ho_tel_3' : $("#edit_ho_tel_3").val(),
            'ho_fax_1' : $("#edit_ho_fax_1").val(),
            'ho_fax_2' : $("#edit_ho_fax_2").val(),
            'ho_email_1' : $("#edit_ho_email_1").val(),
            'ho_email_2' : $("#edit_ho_email_2").val(),
            'status'     : $("#edit_status").val(),
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'banks/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_bank').html('Update');
                $('#update_bank').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_bank').html('Update');
                $('#update_bank').removeAttr('disabled');
            }
        });

    });

    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'banks/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.id);
                $("#edit_code").val(response.code);
                $("#edit_name").val(response.name);
                $("#edit_shortname").val(response.shortname);
                $("#edit_ho_address_1").val(response.ho_address_1);
                $("#edit_ho_address_2").val(response.ho_address_2);
                $("#edit_ho_address_3").val(response.ho_address_3);
                $("#edit_ho_tel_1").val(response.ho_tel_1);
                $("#edit_ho_tel_2").val(response.ho_tel_2);
                $("#edit_ho_tel_3").val(response.ho_tel_3);
                $("#edit_ho_fax_1").val(response.ho_fax_1);
                $("#edit_ho_fax_2").val(response.ho_fax_2);
                $("#edit_ho_email_1").val(response.ho_email_1);
                $("#edit_ho_email_2").val(response.ho_email_2);
                $("#edit_status").val(response.status);

                $('#edit_status')
                    .val(response.status)
                    .trigger("change");

                $('#editBank').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'banks/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    $(".select2").select2();
</script>
@endsection
