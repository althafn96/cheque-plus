@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('app-assets/css/plugins/pickers/daterange/daterange.css') }}">
<style>
    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
    
    div.dt-button-collection {
        max-height: 250px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Scanned Cheques </h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">view cheques scanned by date</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group form-group-style">
                                                    <label for="on_date">Date</label>
                                                    <input data-value="{{date('Y/m/d')}}" name="on_date" type="date" class="form-control pickadate" id="on_date">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered" id="cheques_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th>Cheque No.</th>
                                                    <th>Vendor</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Account No.</th>
                                                    <th>Amount(Rs)</th>
                                                    <th>Deposit Date</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="showChequeimage" tabindex="-1" role="dialog" aria-labelledby="showChequeimageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="showChequeimageModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="imageContent">
                    <div class="mt-3 mb-3" style="text-align: center" id="image_front"></div>
                    <div class="mt-3 mb-3" style="text-align: center" id="image_behind"></div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="returnCheque" tabindex="-1" role="dialog" aria-labelledby="returnChequeModal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="returnChequeModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="rc-content">
                    
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>


<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>

<script>
    
    picker = $('.pickadate').pickadate({
        // Escape any 'rule' characters with an exclamation mark (!).
        format: 'yyyy-mm-dd',
        //formatSubmit: 'yyyy-mm-dd',
        selectYears: true,
        selectMonths: true,
        clear: ''
    });

    var scanned_cheques_table = $('#cheques_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.scanned.index') }}",
            data: function (d) {
                    d.date = $('#on_date').val();
                }
        },
        columns: [
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'bank_branch',
                name: 'bank_branch'
            },
            {
                data: 'account_no',
                name: 'account_no'
            },
            {
                data: 'amount',
                name: 'amount'
            },
            {
                data: 'date',
                name: 'date',
            },
            {
                data: 'received_date',
                name: 'received_date'
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ]
    });

    $('#on_date').change(function() {
        scanned_cheques_table.ajax.reload();
    });


    function show_cheque(id) {

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'scanned/show-cheque-image',
            data: 'id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    $('#showChequeimageModal').html('Cheque No: ' + response.cheque_no);
                    $('#image_front').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_front +`">`);
                    $('#image_behind').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_behind +`">`);
                    $('#showChequeimage').modal('show');
                } else {
                    toastr.error("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }
</script>

@endsection
