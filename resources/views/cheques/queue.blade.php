@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<style>
    .form-control,
    .select2-container,
    input[type=button] {
        font-size: 12px !important;
    }
    
    .select2-selection--single {
        width: 100% !important
    }
    
    input[type=text]:focus, 
    input[type=number]:focus, 
    input[type=button]:focus, 
    input[type=date]:focus {
      background-color: yellow !important
    }
    .select2-container *:focus {
        background-color: yellow !important
    }
    
    input[type=submit]:focus,
    input[type=button]:focus,
    button:focus {
        background-color: yellow !important;
        color: black !important;
    }

    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
    
    div.dt-button-collection {
        max-height: 250px;
        overflow-y: auto;
    }
</style>

@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Manage Cheques in Queue</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item">Update Cheque details in the queue
                        </li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-header mb-0">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" id="save_all_cheques">
                                                    Save Cheques in Queue
                                                </button>
                                                <button type="button" class="btn btn-info btn-min-width btn-glow dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Queue Options</button>
                                                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                                    <button data-defaultType="vendor" class="dropdown-item set_default_in_queue">Set Vendor</button>
                                                    <button data-defaultType="payee" class="dropdown-item set_default_in_queue">Set Payee</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body card-dashboard">
                                        <div id="queue_content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    
    <div class="modal fade text-left" id="queueOptionSet" tabindex="-1" role="dialog" aria-labelledby="queueOptionSetModal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content p-2">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group form-group-style">
                            <label id="queue_set_default_label" for="queue_set_default"></label>
                            <select id="queue_set_default" class="form-control select2">
                                
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-3 offset-md-9 mb-2">
                        <input id="set_default_on_queue" type="button" value="Set" class="btn btn-success float-right">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    $(document).ready(function() {
        
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'load-to-queue',
            dataType: "html",
            success: function(response) {
                if(response == '<div style="text-align: center">No Cheques in Queue</div>') {
                    $('.card-header').html('');
                }
                $('#queue_content').html(response);
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    // on first focus (bubbles up to document), open the menu
    $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
        $(this).closest(".select2-container").siblings('select:enabled').select2('open');
    });

    // steal focus during close - only capture once and stop propogation
    $('select.select2').on('select2:closing', function (e) {
        $(e.target).data("select2").$selection.one('focus focusin', function (e) {
            e.stopPropagation();
        });
    });

    $('#queue_content').on('click', '#next_cheque', function() {
        $('#next_cheque').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#next_cheque').attr('disabled', 'true');
        
        id = $('#next_cheque').attr('data-ID');
        count = $('#next_cheque').attr('data-Count');

        var data = {
            'id' : $('#next_cheque').attr('data-ID'),
            'count' : $('#next_cheque').attr('data-Count')
        };

        var block_ele = $(this).closest('#queue_content');
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            fadeIn: 500,
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#333',
                backgroundColor: 'transparent'
            },
            onBlock: function() {
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: 'load-next-to-queue',
                    data: data,
                    dataType: "html",
                    success: function(response) {
                        $('#queue_content').html(response);
                        $(block_ele).unblock();
                    },
                    error:  function(response) {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $(block_ele).unblock();
                    }
                });
            }
        });
    });

    $('#queue_content').on('click', '#prev_cheque', function() {
        $('#prev_cheque').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#prev_cheque').attr('disabled', 'true');

        id = $('#prev_cheque').attr('data-ID');
        count = $('#prev_cheque').attr('data-Count');

        var data = {
            'id' : $('#prev_cheque').attr('data-ID'),
            'count' : $('#prev_cheque').attr('data-Count')
        };

        var block_ele = $(this).closest('#queue_content');
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            fadeIn: 500,
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#333',
                backgroundColor: 'transparent'
            },
            onBlock: function() {
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: 'load-prev-to-queue',
                    data: data,
                    dataType: "html",
                    success: function(response) {
                        $('#queue_content').html(response);
                        $(block_ele).unblock();
                    },
                    error:  function(response) {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $(block_ele).unblock();
                    }
                });
            }
        });
       
    });

    $('#queue_content').on('click', '#remove_cheque', function() {
        $('#remove_cheque').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#remove_cheque').attr('disabled', 'true');

        // var confirm = confirm("Are you sure you want to remove cheque from queue?"); 
        if (confirm("Are you sure you want to remove cheque from queue?")) { 
            id = $('#remove_cheque').attr('data-ID');

            var data = {
                'id' : $('#remove_cheque').attr('data-ID'),
            };

            var block_ele = $(this).closest('#queue_content');
            $(block_ele).block({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                fadeIn: 500,
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    color: '#333',
                    backgroundColor: 'transparent'
                },
                onBlock: function() {
                    $.ajax({
                        type: "POST",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: 'remove-cheque-from-queue',
                        data: data,
                        dataType: "html",
                        success: function(response) {

                            if(response == 'success') {
                                $(block_ele).unblock();

                                location.reload();
                            } else {
                                toastr.error("Couldn't complete the request. Please contact management");
                                $(block_ele).unblock();
                                $('#remove_cheque').html('Remove cheque from queue');
                                $('#remove_cheque').removeAttr('disabled');
                            }
                        },
                        error:  function(response) {
                            toastr.warning("Couldn't complete the request. Please try again later");
                            $(block_ele).unblock();
                            $('#remove_cheque').html('Remove cheque from queue');
                            $('#remove_cheque').removeAttr('disabled');
                        }
                    });
                }
            });
        } else {
            $('#remove_cheque').html('Remove cheque from queue');
            $('#remove_cheque').removeAttr('disabled');
        }
    
    });

    $('#queue_content').on('change', '#chq_q_list', function() {

        id = $('#chq_q_list').val();

        var block_ele = $(this).closest('#queue_content');
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            fadeIn: 500,
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#333',
                backgroundColor: 'transparent'
            },
            onBlock: function() {
                $.ajax({
                    type: "GET",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: 'load-cheque-from-select',
                    data: 'id='+id,
                    dataType: "html",
                    success: function(response) {
                        $('#queue_content').html(response);
                        $(block_ele).unblock();
                    },
                    error:  function(response) {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $(block_ele).unblock();
                    }
                });
            }
        });
       
    });

    $('#queue_content').on('change', '.ciq_field', function() {
        id = $(this).attr('id');

        var data = {
            'field' : $(this).attr('id'),
            'value' : $(this).val(),
            'id' : $('#ciq_id').val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'save-ciq-field-value',
            data: data,
            dataType: "html",
            success: function(response) {
                $('#chq_q_list').html(response);
            },
            error:  function(response) {

            }
        });
    });

    $('#save_all_cheques').click(function() {
        $('#save_all_cheques').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#save_all_cheques').attr('disabled', 'true');

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'save-cheques-in-queue',
            dataType: "json",
            success: function(response) {
                if(response.type === 'success') {
                    toastr.success(response.text, response.type);
                    $('#save_all_cheques').html('Save Cheques in Queue');
                    $('#save_all_cheques').removeAttr('disabled');

                    $('.card-header').html('');
                    $('#queue_content').html('<div style="text-align: center">No Cheques in Queue</div>');
                    
                    setTimeout(function(){ 
                        window.location.href = "{{ url('cheques/hand') }}";
                    }, 2000);

                }  else {
                    toastr.error(response.text, response.type);
                    $('#save_all_cheques').html('Save Cheques in Queue');
                    $('#save_all_cheques').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#save_all_cheques').html('Save Cheques in Queue');
                $('#save_all_cheques').removeAttr('disabled');
            }
        });
    });

    $('.set_default_in_queue').click(function() {
        type = $(this).attr('data-defaultType');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'get-values-for-setting-queue-default-type',
            data: 'type='+type,
            dataType: "html",
            success: function(response) {
                $('#queue_set_default_label').html('Set ' + type + ' for all cheques in queue');
                $('#set_default_on_queue').attr('data-defaultType', type);
                $('#queue_set_default').html(response);
                $('#queueOptionSet').modal('show');
            },
            error:  function(response) {

            }
        });
    });

    $('#set_default_on_queue').click(function() {
        type = $('#set_default_on_queue').attr('data-defaultType');
        value = $('#queue_set_default').val();

        $('#set_default_on_queue').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#set_default_on_queue').attr('disabled', 'true');

        var data = {
            'type' : type,
            'value' : value
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'set-queue-default',
            data: data,
            dataType: "html",
            success: function(response) {
                $('#set_default_on_queue').html('Set');
                $('#set_default_on_queue').removeAttr('disabled');

                toastr.success(response.text);

                setTimeout(function(){ 
                    location.reload();
                }, 2000);
                
            },
            error:  function(response) {
                $('#set_default_on_queue').html('Set');
                $('#set_default_on_queue').removeAttr('disabled');
            }
        });
    });
</script>
@endsection
