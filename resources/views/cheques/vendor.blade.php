@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<style>
    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
    
    div.dt-button-collection {
        max-height: 250px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Cheques to Vendor </h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">manage cheques to vendor</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered" id="cheques_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th>Granted To (Vendor)</th>
                                                    <th>Cheque No.</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Account No.</th>
                                                    <th>Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Grant Date</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="showChequeimage" tabindex="-1" role="dialog" aria-labelledby="showChequeimageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="showChequeimageModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="imageContent">
                    <div class="mt-3 mb-3" style="text-align: center" id="image_front"></div>
                    <div class="mt-3 mb-3" style="text-align: center" id="image_behind"></div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="returnCheque" tabindex="-1" role="dialog" aria-labelledby="returnChequeModal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="returnChequeModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="rc-content">
                    
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>


<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    var cheques_granted_to_vendor = $('#cheques_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.vendor.index') }}"
        },
        columns: [
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'bank_branch',
                name: 'bank_branch'
            },
            {
                data: 'account_no',
                name: 'account_no',
                visible: false
            },
            {
                data: 'amount',
                name: 'amount'
            },
            {
                data: 'deposit_date',
                name: 'deposit_date',
                visible: false
            },
            {
                data: 'grant_date',
                name: 'grant_date'
            },
            {
                data: 'received_date',
                name: 'received_date',
                visible: false
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'action',
                name: 'action'
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ]
    });

    cheques_granted_to_vendor
    .on( 'select', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = cheques_granted_to_vendor.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            if(!data.includes(item.id)) {
                data += item.id + ',';
            }
        });

        $('#cheques_list').attr('dataChq', data);
    } )
    .on( 'deselect', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = cheques_granted_to_vendor.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            $('#cheques_list').attr('dataChq', data.replace(item.id +",", ""));

            data = $('#cheques_list').attr('dataChq');
        });
    } );


    function show_cheque(id) {

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'show-cheque-image',
            data: 'id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    $('#showChequeimageModal').html('Cheque No: ' + response.cheque_no);
                    $('#image_front').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_front +`">`);
                    $('#image_behind').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_behind +`">`);
                    $('#showChequeimage').modal('show');
                } else {
                    toastr.error("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }
</script>

@endsection
