@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<style>
    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
    
    div.dt-button-collection {
        max-height: 250px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Cheques to Bank </h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">manage cheques in bank, mark and manage return cheques</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-header">
                                        <h4 class="card-title">Cheques to Bank</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                            <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                                <button id="return_selected_to_bank" class="dropdown-item">Mark Return</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered" id="cheques_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Vendor</th>
                                                    <th>Cheque No.</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Account No.</th>
                                                    <th>Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Grant Date</th>
                                                    <th>Granted To</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-header">
                                        <h4 class="card-title">Return Cheques</h4>
                                        
                                    </div>
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered" id="return_cheques_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Vendor</th>
                                                    <th>Cheque No.</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Return Reason</th>
                                                    <th>Return Date</th>
                                                    <th>Account No.</th>
                                                    <th>Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Grant Date</th>
                                                    <th>Granted To</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="showChequeimage" tabindex="-1" role="dialog" aria-labelledby="showChequeimageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="showChequeimageModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="imageContent">
                    <div class="mt-3 mb-3" style="text-align: center" id="image_front"></div>
                    <div class="mt-3 mb-3" style="text-align: center" id="image_behind"></div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="returnCheque" tabindex="-1" role="dialog" aria-labelledby="returnChequeModal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="returnChequeModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="rc-content">
                    
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>


<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    var cheques_granted_to_bank = $('#cheques_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.bank.index') }}"
        },
        columns: [
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'bank_branch',
                name: 'bank_branch'
            },
            {
                data: 'account_no',
                name: 'account_no',
                visible: false
            },
            {
                data: 'amount',
                name: 'amount',
                visible: false
            },
            {
                data: 'deposit_date',
                name: 'deposit_date'
            },
            {
                data: 'grant_date',
                name: 'grant_date'
            },
            {
                data: 'granted_to',
                name: 'granted_to'
            },
            {
                data: 'received_date',
                name: 'received_date',
                visible: false
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            },
            'selectAll',
            'selectNone'
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            },
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ],
        select: {
            style:    'os',
            selector: 'td:first-child',
            style: 'multi'
        },
        "drawCallback": function( settings ) {
            var existing = $('#cheques_list').attr('dataChq');
            
            cheques_granted_to_bank.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                var data = this.data();
                
                if(existing.includes(data.id)) {
                    this.select();
                }
            } );
        }
    });

    cheques_granted_to_bank
    .on( 'select', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = cheques_granted_to_bank.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            if(!data.includes(item.id)) {
                data += item.id + ',';
            }
        });

        $('#cheques_list').attr('dataChq', data);
    } )
    .on( 'deselect', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = cheques_granted_to_bank.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            $('#cheques_list').attr('dataChq', data.replace(item.id +",", ""));

            data = $('#cheques_list').attr('dataChq');
        });
    } );

    var return_cheques = $('#return_cheques_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.bank-return.index') }}"
        },
        columns: [
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'bank_branch',
                name: 'bank_branch'
            },
            {
                data: 'rr_title',
                name: 'return_reason'
            },
            {
                data: 'returned_date',
                name: 'return_date'
            },
            {
                data: 'account_no',
                name: 'account_no',
                visible: false
            },
            {
                data: 'amount',
                name: 'amount',
                visible: false
            },
            {
                data: 'deposit_date',
                name: 'deposit_date',
                visible: false
            },
            {
                data: 'grant_date',
                name: 'grant_date',
                visible: false
            },
            {
                data: 'granted_to',
                name: 'granted_to',
                visible: false
            },
            {
                data: 'received_date',
                name: 'received_date',
                visible: false
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            },
            'selectAll',
            'selectNone'
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            },
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ],
        select: {
            style:    'os',
            selector: 'td:first-child',
            style: 'multi'
        }
    });

    return_cheques
    .on( 'select', function ( e, dt, type, indexes ) {
        var data = $('#return_cheques_list').attr('dataChq');
        var rowData = return_cheques.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            if(!data.includes(item.id)) {
                data += item.id + ',';
            }
        });

        $('#return_cheques_list').attr('dataChq', data);
    } )
    .on( 'deselect', function ( e, dt, type, indexes ) {
        var data = $('#return_cheques_list').attr('dataChq');
        var rowData = return_cheques.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            $('#return_cheques_list').attr('dataChq', data.replace(item.id +",", ""));

            data = $('#return_cheques_list').attr('dataChq');
        });
    } );

    function mark_return(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'mark-return',
            data: 'selected_ids='+id,
            dataType: "html",
            success: function(response) {
                if(response == 'Please select a cheque/cheques to grant to bank') {
                    toastr.error("Please select a cheque/cheques to grant to bank");
                } else {
                    $('#rc-content').html(response);
                    $('#returnCheque').modal('show');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function release_cheque(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'release-cheque',
            data: 'chq_id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    cheques_granted_to_bank.ajax.reload();
                    return_cheques.ajax.reload();
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    $('#hold_selected_cheques').click(function() {
        var selected_ids = $('#cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'hold-selected-cheques',
            data: 'selected_ids='+selected_ids,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    cheques_granted_to_bank.ajax.reload();
                    return_cheques.ajax.reload();

                    $('#cheques_list').attr('dataChq', '');
                } else {
                    toastr.error(response.text);
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    $('#release_selected_cheques').click(function() {
        var selected_ids = $('#return_cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'release-selected-cheques',
            data: 'selected_ids='+selected_ids,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    cheques_granted_to_bank.ajax.reload();
                    return_cheques.ajax.reload();

                    $('#return_cheques_list').attr('dataChq', '');
                } else {
                    toastr.error(response.text);
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    function show_cheque(id) {

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'show-cheque-image',
            data: 'id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    $('#showChequeimageModal').html('Cheque No: ' + response.cheque_no);
                    $('#image_front').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_front +`">`);
                    $('#image_behind').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_behind +`">`);
                    $('#showChequeimage').modal('show');
                } else {
                    toastr.error("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    $('#return_selected_to_bank').click(function() {
        var selected_ids = $('#cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'mark-return',
            data: 'selected_ids='+selected_ids+"&type=bank",
            dataType: "html",
            success: function(response) {
                if(response == 'Please select a cheque/cheques to grant to bank') {
                    toastr.error("Please select a cheque/cheques to grant to bank");
                } else {
                    $('#rc-content').html(response);
                    $('#returnCheque').modal('show');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    $('#returnCheque').on('change', '#cheque_no_on_return_queue', function() {

        id = $('#cheque_no_on_return_queue').val();

        $('#chq_list_details').block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            fadeIn: 500,
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#333',
                backgroundColor: 'transparent'
            },
            onBlock: function() {
                $('#chq_images').block({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    fadeIn: 100,
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        color: '#333',
                        backgroundColor: 'transparent'
                    }
                });

                var data = {
                    'id' : $('#cheque_no_on_return_queue').val(),
                };

                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: 'load-selected-cheque-in-grant-queue',
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        if(response.type == 'success') {
                            $('#chq_list_details').html(response.chq_list_details);
                            $('#chq_images').html(response.chq_images);
                            $('.rc_queue').unblock();
                        } else {
                            toastr.warning("Couldn't complete the request. Please contact management");
                            $('.rc_queue').unblock();
                        }
                    },
                    error:  function(response) {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('.rc_queue').unblock();
                    }
                });
            }
        });
       
    });

    $('#returnCheque').on('click', '#submit_return_cheques', function() {
        $('#submit_return_cheques').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_return_cheques').attr('disabled', 'true');

        var data = {
            'selected_ids' : $('#submit_return_cheques').attr('dataList'),
            'return_date' : $('#return_date').val(),
            'return_reason' : $('#return_reason').val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'submit-return-cheques',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);
                    $('#submit_return_cheques').html('Submit');
                    $('#submit_return_cheques').removeAttr('disabled');

                    setTimeout(function(){ 
                        window.location.href = "{{ url('cheques/banks') }}";
                    }, 2000);
                    
                } else {
                    toastr.error(response.text);
                    $('#submit_return_cheques').html('Submit');
                    $('#submit_return_cheques').removeAttr('disabled');
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_return_cheques').html('Submit');
                $('#submit_return_cheques').removeAttr('disabled');
            }
        });
    });

</script>

@endsection
