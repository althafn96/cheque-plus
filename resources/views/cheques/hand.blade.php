@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
<style>
    .select2-dropdown, .select2-selection__rendered{
        font-size: 11px !important
    }
    
    div.dt-button-collection {
        max-height: 250px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Manage Cheques</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">hold, release, grant cheque to vendor or bank</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-header">
                                        <h4 class="card-title">Cheques in Hand (Released)</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                            <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                                <button id="grant_selected_to_bank" class="dropdown-item">Grant to Bank</button>
                                                <button id="grant_selected_to_vendor" class="dropdown-item">Grant to Vendor</button>
                                                <button id="hold_selected_cheques" class="dropdown-item">Hold Cheques</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body card-dashboard">
                                        <div class="row">
                                            <div class="col-md-3 offset-md-6">
                                                <fieldset class="form-group form-group-style">
                                                    <label for="payee">Filter by Payee</label>
                                                    <select name="payee" id="payee" class="select2 form-control">
                                                        <option value="0">-- SELECT PAYEE --</option>
                                                        @foreach ($branches as $branch)
                                                            <option value="{{ $branch->id }}">{{ $branch->brnch_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                                <fieldset class="form-group form-group-style">
                                                    <label for="vendor">Filter by Vendor</label>
                                                    <select name="vendor" id="vendor" class="select2 form-control">
                                                        <option value="0">-- SELECT VENDOR --</option>
                                                        @foreach ($vendors as $vendor)
                                                            <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered" id="cheques_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Vendor</th>
                                                    <th>Cheque No.</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Account No.</th>
                                                    <th>Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-header">
                                        <h4 class="card-title">Cheques on Hold</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                            <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                                <button id="release_selected_cheques" class="dropdown-item" >Release Cheques</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered" id="cheques_on_hold_list" dataChq=''>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Vendor</th>
                                                    <th>Cheque No.</th>
                                                    <th>Bank (Branch)</th>
                                                    <th>Account No.</th>
                                                    <th>Amount</th>
                                                    <th>Deposit Date</th>
                                                    <th>Received Date</th>
                                                    <th>Company Branch</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="showChequeimage" tabindex="-1" role="dialog" aria-labelledby="showChequeimageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="showChequeimageModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="imageContent" class="imageContent">
                    <div class="mt-3 mb-3" style="text-align: center" id="image_front"></div>
                    <div class="mt-3 mb-3" style="text-align: center" id="image_behind"></div>
                </div>
                <button id="print_cheque" type="button" class="btn btn-primary float-right m-2">Print</button>
                
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="grantCheque" tabindex="-1" role="dialog" aria-labelledby="grantChequeModal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="grantChequeModal"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="gc-content">
                    
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>


<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script src="{{ asset('app-assets/vendors/js/printThis/printThis.js') }}"></script>

<script>
    $('.select2').select2();

    var released_cheques_table = $('#cheques_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.hand.index') }}",
            data: function (d) {
                    d.payee = $('#payee').val(),
                    d.vendor = $('#vendor').val()
            }
        },
        columns: [
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'bank_branch',
                name: 'bank_branch'
            },
            {
                data: 'account_no',
                name: 'account_no'
            },
            {
                data: 'amount',
                name: 'amount'
            },
            {
                data: 'deposit_date',
                name: 'deposit_date',
            },
            {
                data: 'received_date',
                name: 'received_date'
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            },
            'selectAll',
            'selectNone'
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            },
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ],
        select: {
            style:    'os',
            selector: 'td:first-child',
            style: 'multi'
        },
        "drawCallback": function( settings ) {
            var existing = $('#cheques_list').attr('dataChq');
            
            released_cheques_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                var data = this.data();
                
                if(existing.includes(data.id)) {
                    this.select();
                }
            } );
        }
    });

    released_cheques_table
    .on( 'select', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = released_cheques_table.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            if(!data.includes(item.id)) {
                data += item.id + ',';
            }
        });

        $('#cheques_list').attr('dataChq', data);
    } )
    .on( 'deselect', function ( e, dt, type, indexes ) {
        var data = $('#cheques_list').attr('dataChq');
        var rowData = released_cheques_table.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            $('#cheques_list').attr('dataChq', data.replace(item.id +",", ""));

            data = $('#cheques_list').attr('dataChq');
        });
    } );

    var cheques_on_hold = $('#cheques_on_hold_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('cheques.hand-hold.index') }}"
        },
        columns: [
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'vendor_id',
                name: 'vendor_id'
            },
            {
                data: 'cheque_no',
                name: 'cheque_no'
            },
            {
                data: 'bank_id',
                name: 'bank_id'
            },
            {
                data: 'account_no',
                name: 'account_no'
            },
            {
                data: 'amount',
                name: 'amount'
            },
            {
                data: 'deposit_date',
                name: 'deposit_date',
            },
            {
                data: 'received_date',
                name: 'received_date'
            },
            {
                data: 'payee_id',
                name: 'payee_id',
                visible: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            },
            {
                data: 'res',
                name: 'res'
            }
        ],
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        stateSave: true,
        buttons: [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            },
            'selectAll',
            'selectNone'
        ],       
        columnDefs: [ 
            {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            },
            {
                orderable: false,
                className: 'control',
                targets:   -1
            },
            {
                targets: 0,
                className: 'noVis'
            },
            {
                targets: -1,
                className: 'noVis'
            }
         ],
        select: {
            style:    'os',
            selector: 'td:first-child',
            style: 'multi'
        },
        "drawCallback": function( settings ) {
            var existing = $('#cheques_on_hold_list').attr('dataChq');
            
            cheques_on_hold.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                var data = this.data();
                
                if(existing.includes(data.id)) {
                    this.select();
                }
            } );
        }
    });

    cheques_on_hold
    .on( 'select', function ( e, dt, type, indexes ) {
        var data = $('#cheques_on_hold_list').attr('dataChq');
        var rowData = cheques_on_hold.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            if(!data.includes(item.id)) {
                data += item.id + ',';
            }
        });

        $('#cheques_on_hold_list').attr('dataChq', data);
    } )
    .on( 'deselect', function ( e, dt, type, indexes ) {
        var data = $('#cheques_on_hold_list').attr('dataChq');
        var rowData = cheques_on_hold.rows( indexes ).data().toArray();

        rowData.forEach(function myFunction(item, index) {
            $('#cheques_on_hold_list').attr('dataChq', data.replace(item.id +",", ""));

            data = $('#cheques_on_hold_list').attr('dataChq');
        });
    } );

    function hold_cheque(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'hold-cheque',
            data: 'chq_id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);
                    released_cheques_table.ajax.reload();
                    cheques_on_hold.ajax.reload();
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function release_cheque(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'release-cheque',
            data: 'chq_id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    released_cheques_table.ajax.reload();
                    cheques_on_hold.ajax.reload();
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    $('#hold_selected_cheques').click(function() {
        var selected_ids = $('#cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'hold-selected-cheques',
            data: 'selected_ids='+selected_ids,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    released_cheques_table.ajax.reload();
                    cheques_on_hold.ajax.reload();

                    $('#cheques_list').attr('dataChq', '');
                } else {
                    toastr.error(response.text);
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    $('#release_selected_cheques').click(function() {
        var selected_ids = $('#cheques_on_hold_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'release-selected-cheques',
            data: 'selected_ids='+selected_ids,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    toastr.success(response.text);

                    released_cheques_table.ajax.reload();
                    cheques_on_hold.ajax.reload();

                    $('#cheques_on_hold_list').attr('dataChq', '');
                } else {
                    toastr.error(response.text);
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    function show_cheque(id) {

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'show-cheque-image',
            data: 'id='+id,
            dataType: "json",
            success: function(response) {
                if(response.type === 'success'){
                    $('#showChequeimageModal').html('Cheque No: ' + response.cheque_no);
                    $('#image_front').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_front +`">`);
                    $('#image_behind').html(`<img style="width: 90%" src="{{ asset('chq_source') }}/`+ response.image_behind +`">`);
                    $('#showChequeimage').modal('show');
                } else {
                    toastr.error("Couldn't complete the request. Please contact management");
                }

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    $('#grant_selected_to_bank').click(function() {
        var selected_ids = $('#cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'grant-cheques',
            data: 'selected_ids='+selected_ids+"&type=bank",
            dataType: "html",
            success: function(response) {
                if(response == 'Please select a cheque/cheques to grant to bank') {

                    toastr.error("Please select a cheque/cheques to grant to bank");

                } else if(response == 'multiple banks') {

                    toastr.error("Multiple banks selected. Please select cheques pertaining to only 1 bank since cheques cannot be granted to multiple banks at once");
                
                } else {
                    $('#gc-content').html(response);
                    $('#grantCheque').modal('show');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    $('#grant_selected_to_vendor').click(function() {
        var selected_ids = $('#cheques_list').attr('dataChq');

        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'grant-cheques',
            data: 'selected_ids='+selected_ids+"&type=vendor",
            dataType: "html",
            success: function(response) {
                if(response == 'Please select a cheque/cheques to grant to vendor') {

                    toastr.error("Please select a cheque/cheques to grant to vendor");

                } else if(response == 'multiple vendors') {

                    toastr.error("Multiple vendors selected. Please select cheques pertaining to only 1 vendor since cheques cannot be granted to multiple vendors at once");
                
                } else {
                    $('#gc-content').html(response);
                    $('#grantCheque').modal('show');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    });

    $('#grantCheque').on('change', '#cheque_no_on_grant_queue', function() {

        id = $('#cheque_no_on_grant_queue').val();

        $('#chq_list_details').block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            fadeIn: 500,
            overlayCSS: {
                backgroundColor: '#FFF',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#333',
                backgroundColor: 'transparent'
            },
            onBlock: function() {
                $('#chq_images').block({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    fadeIn: 100,
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        color: '#333',
                        backgroundColor: 'transparent'
                    }
                });

                var data = {
                    'id' : $('#cheque_no_on_grant_queue').val(),
                };

                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: 'load-selected-cheque-in-grant-queue',
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        if(response.type == 'success') {
                            $('#chq_list_details').html(response.chq_list_details);
                            $('#chq_images').html(response.chq_images);
                            $('.grant_queue').unblock();
                        } else {
                            toastr.warning("Couldn't complete the request. Please contact management");
                            $('.grant_queue').unblock();
                        }
                    },
                    error:  function(response) {
                        toastr.warning("Couldn't complete the request. Please contact management");
                        $('.grant_queue').unblock();
                    }
                });
            }
        });
       
    });

    $('#grantCheque').on('click', '#submit_granting_cheques', function() {
        $('#submit_granting_cheques').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_granting_cheques').attr('disabled', 'true');

        var data = {
            'selected_ids' : $('#submit_granting_cheques').attr('dataList'),
            'type' : $('#submit_granting_cheques').attr('dataType'),
            'grant_date' : $('#grant_date').val(),
            'grant_type_id' : $('#grant_type_id').val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'submit-grant-cheques',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);
                    $('#submit_granting_cheques').html('Submit');
                    $('#submit_granting_cheques').removeAttr('disabled');

                    setTimeout(function(){ 
                        window.location.href = "{{ url('cheques') }}/"+$('#submit_granting_cheques').attr('dataType')+'s';
                    }, 2000);
                    
                } else {
                    toastr.error(response.text);
                    $('#submit_granting_cheques').html('Submit');
                    $('#submit_granting_cheques').removeAttr('disabled');
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_granting_cheques').html('Submit');
                $('#submit_granting_cheques').removeAttr('disabled');
            }
        });
    });

    $('#print_cheque').click(function(){
        $('#imageContent').printThis();
    });

    $('#payee').change(function() {
        released_cheques_table.ajax.reload();
    });

    $('#vendor').change(function() {
        released_cheques_table.ajax.reload();
    });

</script>

@endsection
