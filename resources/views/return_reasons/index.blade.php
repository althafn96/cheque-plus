@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">


@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Return Reasons</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add, Update or Delete Return Reasons
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Return Reasons</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createReturnReason">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createReturnReason" role="dialog" aria-labelledby="createReturnReasonModal" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createReturnReasonModal">Create Branch</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_bank_form">
											  	  <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="title">Title <span class="red">*</span></label>
                                                                    <input type="text" class="form-control return_reasons" id="title" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="code">Code <span class="red">*</span></label>
                                                                    <input type="text" class="form-control return_reasons" id="code" >
                                                                </fieldset>
                                                            </div>
                                                        </div>
												  </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_rr" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="return_reasons_list">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Code</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>

    <div class="modal fade text-left" id="editReturnReason" role="dialog" aria-labelledby="editReturnReasonModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editReturnReasonModal">Edit Bank Branch</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="edit_bank_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_title">Title <span class="red">*</span></label>
                                    <input type="text" class="form-control return_reasons" id="edit_title" >
                                    <input type="hidden" class="form-control return_reasons" id="edit_id" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_code">Code <span class="red">*</span></label>
                                    <input type="text" class="form-control return_reasons" id="edit_code" >
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_rr" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script>
    var table = $('#return_reasons_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('return-reasons.index') }}"
        },
        columns: [
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'code',
                name: 'code'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_rr').on('click',function() {
        $('#submit_rr').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_rr').attr('disabled', 'true');

        var data = {
            'title' : $("#title").val(),
            'code' : $("#code").val()
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("return-reasons.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_rr').html('Save');
                    $('#submit_rr').removeAttr('disabled');

                    $("#title").val('');
                    $("#code").val('');

                    setTimeout(function(){ 
                        $('#createReturnReason').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_rr').html('Save');
                    $('#submit_rr').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_rr').html('Save');
                    $('#submit_rr').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_rr').html('Save');
                $('#submit_rr').removeAttr('disabled');
            }
        });

    });

    $('#update_rr').on('click',function() {
        $('#update_rr').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_rr').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'title' : $("#edit_title").val(),
            'code' : $("#edit_code").val()
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'return-reasons/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_rr').html('Update');
                $('#update_rr').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_rr').html('Update');
                $('#update_rr').removeAttr('disabled');
            }
        });

    });


    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'return-reasons/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.id);
                $("#edit_title").val(response.title);
                $("#edit_code").val(response.code);

                $('#editReturnReason').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'return-reasons/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

</script>
@endsection
