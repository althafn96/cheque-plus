@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">


@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Bank Branches</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add, Update or Delete Bank Branches
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Bank Branches</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createBankBranch">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createBankBranch" role="dialog" aria-labelledby="createBankBranchModal" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createBankBranchModal">Create Bank Branch</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_bank_form">
											  	  <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_code">Branch Code <span class="red">*</span></label>
                                                                <input type="text" class="form-control bb_field" id="bb_code" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_name">Branch Name <span class="red">*</span></label>
                                                                <input type="text" class="form-control bb_field" id="bb_name" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_bank">Bank</label>
                                                                <select id="bb_bank" class="form-control">
                                                                    <option value="0">--Select Bank--</option>
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_address_1">Address 1 <span class="red">*</span></label>
                                                                <input type="text" class="form-control bb_field" id="bb_address_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_address_2">Address 2</label>
                                                                <input type="text" class="form-control bb_field" id="bb_address_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_address_3">Address 3</label>
                                                                <input type="text" class="form-control bb_field" id="bb_address_3" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_tel_1">Telephone No (1) <span class="red">*</span></label>
                                                                <input type="text" class="form-control bb_field" id="bb_tel_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_tel_2">Telephone No (2)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_tel_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_tel_3">Telephone No (3)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_tel_3" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_fax_1">Fax No (1)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_fax_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_fax_2">Fax No (2)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_fax_2" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_email_1">Email (1)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_email_1" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="bb_email_2">Email (2)</label>
                                                                <input type="text" class="form-control bb_field" id="bb_email_2" >
                                                            </fieldset>
                                                        </div>
                                                    </div>
												  </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_bank_branch" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="bank_branches_list">
                                        <thead>
                                            <tr>
                                                <th>Branch Code</th>
                                                <th>Branch Name</th>
                                                <th>Bank</th>
                                                <th>Address</th>
                                                <th>Telephone</th>
                                                <th>Email</th>
                                                <th>Fax</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>

    <div class="modal fade text-left" id="editBankBranch" tabindex="-1" role="dialog" aria-labelledby="editBankBranchModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editBankBranchModal">Edit Bank Branch</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="edit_bank_form">
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_code">Bank Code <span class="red">*</span></label>
                                <input type="text" class="form-control bb_field" id="edit_bb_code" >
                                <input type="hidden" class="form-control bb_field" id="edit_id" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_name">Bank Name <span class="red">*</span></label>
                                <input type="text" class="form-control bb_field" id="edit_bb_name" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bank_id">Bank</label>
                                <select id="edit_bank_id" class="select2 form-control">
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_address_1">Address 1 <span class="red">*</span></label>
                                <input type="text" class="form-control bb_field" id="edit_bb_address_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_address_2">Address 2</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_address_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_address_3">Address 3</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_address_3" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_tel_1">Telephone No (1) <span class="red">*</span></label>
                                <input type="text" class="form-control bb_field" id="edit_bb_tel_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_tel_2">Telephone No (2)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_tel_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_tel_3">Telephone No (3)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_tel_3" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_fax_1">Fax No (1)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_fax_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_fax_2">Fax No (2)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_fax_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_email_1">Email (1)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_email_1" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_bb_email_2">Email (2)</label>
                                <input type="text" class="form-control bb_field" id="edit_bb_email_2" >
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="edit_status">Status</label>
                                <select id="edit_status" class="select2 form-control">
                                    <option value="0">Disable</option>
                                    <option value="1">Enable</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_bank_branch" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script>
    var table = $('#bank_branches_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('bank-branches.index') }}"
        },
        columns: [
            {
                data: 'bb_code',
                name: 'bb_code'
            },
            {
                data: 'bb_name',
                name: 'bb_name'
            },
            {
                data: 'bank',
                name: 'bank'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.bb_address_1 == 'null') {
                        data.bb_address_1 = '';
                    }
                    if(data.bb_address_2 == 'null') {
                        data.bb_address_2 = '';
                    }
                    if(data.bb_address_3 == 'null') {
                        data.bb_address_3 = '';
                    }
                    return data.bb_address_1 + "<br/>" + data.bb_address_2 + "<br/>" + data.bb_address_3;
                },
                name: 'address',
                visible: false
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.bb_tel_1 == null) {
                        data.bb_tel_1 = '';
                    }
                    if(data.bb_tel_2 == null) {
                        data.bb_tel_2 = '';
                    }
                    if(data.bb_tel_3 == null) {
                        data.bb_tel_3 = '';
                    }
                    return "<a href='tel:"+ data.bb_tel_1 +"'>" + data.bb_tel_1 + "</a><br/><a href='tel:"+ data.bb_tel_2 +"'>" + data.bb_tel_2 + "</a><br/><a href='tel:"+ data.bb_tel_3 +"'>" + data.bb_tel_3;
                },
                name: 'telephone'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.bb_email_1 == null) {
                        data.bb_email_1 = '';
                    }
                    if(data.bb_email_2 == null) {
                        data.bb_email_2 = '';
                    }
                    
                    return "<a href='mailto:"+ data.bb_email_1 +"'>" + data.bb_email_1 + "</a><br/><a href='mailto:"+ data.bb_email_2 +"'>" + data.bb_email_2;
                },
                name: 'email'
            },
            {
                data: function (data, type, dataToSet) {
                    if(data.bb_fax_1 == null) {
                        data.bb_fax_1 = '';
                    }
                    if(data.bb_fax_2 == null) {
                        data.bb_fax_2 = '';
                    }
                    
                    return data.bb_fax_1 + "<br/>"+ data.bb_fax_2;
                },
                name: 'fax',
                visible: false
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.status == '1') {
                        return '<div class="badge badge-success">Enabled</div>';
                    } else {
                        return '<div class="badge badge-danger">Disabled</div>';
                    }
                    
                },
                name: 'status'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_bank_branch').on('click',function() {
        $('#submit_bank_branch').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_bank_branch').attr('disabled', 'true');

        var data = {
            'bb_code' : $("#bb_code").val(),
            'bb_name' : $("#bb_name").val(),
            'bb_bank' : $("#bb_bank").val(),
            'bb_address_1' : $("#bb_address_1").val(),
            'bb_address_2' : $("#bb_address_2").val(),
            'bb_address_3' : $("#bb_address_3").val(),
            'bb_tel_1' : $("#bb_tel_1").val(),
            'bb_tel_2' : $("#bb_tel_2").val(),
            'bb_tel_3' : $("#bb_tel_3").val(),
            'bb_fax_1' : $("#bb_fax_1").val(),
            'bb_fax_2' : $("#bb_fax_2").val(),
            'bb_email_1' : $("#bb_email_1").val(),
            'bb_email_2' : $("#bb_email_2").val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("bank-branches.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_bank_branch').html('Save');
                    $('#submit_bank_branch').removeAttr('disabled');

                    $("#bb_code").val('');
                    $("#bb_name").val('');
                    $("#bb_bank").val('');
                    $("#bb_address_1").val('');
                    $("#bb_address_2").val('');
                    $("#bb_address_3").val('');
                    $("#bb_tel_1").val('');
                    $("#bb_tel_2").val('');
                    $("#bb_tel_3").val('');
                    $("#bb_fax_1").val('');
                    $("#bb_fax_2").val('');
                    $("#bb_email_1").val('');
                    $("#bb_email_2").val('');

                    setTimeout(function(){ 
                        $('#createBankBranch').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_bank_branch').html('Save');
                    $('#submit_bank_branch').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_bank_branch').html('Save');
                    $('#submit_bank_branch').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_bank_branch').html('Save');
                $('#submit_bank_branch').removeAttr('disabled');
            }
        });

    });

    $('#update_bank_branch').on('click',function() {
        $('#update_bank_branch').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_bank_branch').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'bb_code' : $("#edit_bb_code").val(),
            'bb_name' : $("#edit_bb_name").val(),
            'bank_id' : $("#edit_bank_id").val(),
            'bb_address_1' : $("#edit_bb_address_1").val(),
            'bb_address_2' : $("#edit_bb_address_2").val(),
            'bb_address_3' : $("#edit_bb_address_3").val(),
            'bb_tel_1' : $("#edit_bb_tel_1").val(),
            'bb_tel_2' : $("#edit_bb_tel_2").val(),
            'bb_tel_3' : $("#edit_bb_tel_3").val(),
            'bb_fax_1' : $("#edit_bb_fax_1").val(),
            'bb_fax_2' : $("#edit_bb_fax_2").val(),
            'bb_email_1' : $("#edit_bb_email_1").val(),
            'bb_email_2' : $("#edit_bb_email_2").val(),
            'status' : $("#edit_status").val(),
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'bank-branches/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_bank_branch').html('Update');
                $('#update_bank_branch').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_bank_branch').html('Update');
                $('#update_bank_branch').removeAttr('disabled');
            }
        });

    });

    $(".select2").select2();

    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'bank-branches/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.bankBranch.id);
                $("#edit_bb_code").val(response.bankBranch.bb_code);
                $("#edit_bb_name").val(response.bankBranch.bb_name);
                $("#edit_bank_id").html(response.banks);
                $("#edit_bb_address_1").val(response.bankBranch.bb_address_1);
                $("#edit_bb_address_2").val(response.bankBranch.bb_address_2);
                $("#edit_bb_address_3").val(response.bankBranch.bb_address_3);
                $("#edit_bb_tel_1").val(response.bankBranch.bb_tel_1);
                $("#edit_bb_tel_2").val(response.bankBranch.bb_tel_2);
                $("#edit_bb_tel_3").val(response.bankBranch.bb_tel_3);
                $("#edit_bb_fax_1").val(response.bankBranch.bb_fax_1);
                $("#edit_bb_fax_2").val(response.bankBranch.bb_fax_2);
                $("#edit_bb_email_1").val(response.bankBranch.bb_email_1);
                $("#edit_bb_email_2").val(response.bankBranch.bb_email_2);
                $("#edit_status").val(response.bankBranch.status);

                $('#edit_status')
                    .val(response.bankBranch.status)
                    .trigger("change");

                $('#editBankBranch').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'bank-branches/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }
    
    $("#bb_bank").select2({
        ajax: {
            url: 'bank-branches/fetch-banks',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            },
            
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        },
        width: '100%'
    });

</script>
@endsection
