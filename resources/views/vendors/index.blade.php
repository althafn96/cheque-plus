@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">
@endsection

@section('content')
 <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Manage Vendors</h3>
                <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Add, Update or Delete Vendors
                    </li>
                    </ol>
                </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Vendors</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createVendor">
										Add
                                    </button>
                                    <div class="modal fade text-left" id="createVendor" tabindex="-1" role="dialog" aria-labelledby="createVendorModal" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<label class="modal-title text-text-bold-600" id="createVendorModal">Create Vendor</label>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form class="add_vendor_form">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="code">Vendor Code</label>
                                                                <input type="text" class="form-control vendor_field" id="code" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="name">Vendor Name <span class="red">*</span></label>
                                                                <input type="text" class="form-control vendor_field" id="name" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="other_name">Other Name(s)</label>
                                                                <input type="text" class="form-control vendor_field" id="other_name" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="address">Address</label>
                                                                <input type="text" class="form-control vendor_field" id="address" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="primary_contact_no">Primary Contact No.</label>
                                                                <input type="text" class="form-control vendor_field" id="primary_contact_no" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="secondary_contact_no">Secondary Contact No.</label>
                                                                <input type="text" class="form-control vendor_field" id="secondary_contact_no" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="email">Email</label>
                                                                <input type="text" class="form-control vendor_field" id="email" >
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <fieldset class="form-group form-group-style">
                                                                <label for="fax">Fax</label>
                                                                <input type="text" class="form-control vendor_field" id="fax" >
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
												  <div class="modal-footer">
													<button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
													<button type="button" id="submit_vendor" class="btn btn-success btn-glow"> Save </button>
												  </div>
												</form>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="vendors_list">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Other Name(s)</th>
                                                <th>Address</th>
                                                <th>Primary Contact No.</th>
                                                <th>Secondary Contact No.</th>
                                                <th>Email</th>
                                                <th>Fax</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
  </div>

    <div class="modal fade text-left" id="editVendor" tabindex="-1" role="dialog" aria-labelledby="editVendorModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editVendorModal">Edit Vendor</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="add_vendor_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_code">Vendor Code</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_code" >
                                    <input type="hidden" class="form-control edit_vendor_field" id="edit_id" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_name">Vendor Name <span class="red">*</span></label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_name" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_other_name">Other Name(s)</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_other_name" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_address">Address</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_address" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_primary_contact_no">Primary Contact No.</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_primary_contact_no" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_secondary_contact_no">Secondary Contact No.</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_secondary_contact_no" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_email">Email</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_email" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_fax">Fax</label>
                                    <input type="text" class="form-control edit_vendor_field" id="edit_fax" >
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_vendor" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script>
    var table = $('#vendors_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('vendors.index') }}"
        },
        columns: [
            {
                data: 'code',
                name: 'code'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'other_name',
                name: 'other_name',
                visible: false
            },
            {
                data: 'address',
                name: 'address'
            },
            {
                data: 'primary_contact_no',
                name: 'primary_contact_no'
            },
            {
                data: 'secondary_contact_no',
                name: 'secondary_contact_no',
                visible: false
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'fax',
                name: 'fax',
                visible: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_vendor').on('click',function() {
        $('#submit_vendor').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_vendor').attr('disabled', 'true');

        var data = {
            'code' : $("#code").val(),
            'name' : $("#name").val(),
            'other_name' : $("#other_name").val(),
            'address' : $("#address").val(),
            'primary_contact_no' : $("#primary_contact_no").val(),
            'secondary_contact_no' : $("#secondary_contact_no").val(),
            'email' : $("#email").val(),
            'fax' : $("#fax").val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("vendors.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_vendor').html('Save');
                    $('#submit_vendor').removeAttr('disabled');

                    $("#code").val('');
                    $("#name").val('');
                    $("#other_name").val('');
                    $("#address").val('');
                    $("#primary_contact_no").val('');
                    $("#secondary_contact_no").val('');
                    $("#email").val('');
                    $("#fax").val('');

                    setTimeout(function(){ 
                        $('#createVendor').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_vendor').html('Save');
                    $('#submit_vendor').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_vendor').html('Save');
                    $('#submit_vendor').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });

    });

    $('#update_vendor').on('click',function() {
        $('#update_vendor').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_vendor').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'code' : $("#edit_code").val(),
            'name' : $("#edit_name").val(),
            'other_name' : $("#edit_other_name").val(),
            'address' : $("#edit_address").val(),
            'primary_contact_no' : $("#edit_primary_contact_no").val(),
            'secondary_contact_no' : $("#edit_secondary_contact_no").val(),
            'email' : $("#edit_email").val(),
            'fax' : $("#edit_fax").val(),
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'vendors/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_vendor').html('Update');
                $('#update_vendor').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_vendor').html('Update');
                $('#update_vendor').removeAttr('disabled');
            }
        });

    });

    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'vendors/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.id);
                $("#edit_code").val(response.code);
                $("#edit_name").val(response.name);
                $("#edit_other_name").val(response.other_name);
                $("#edit_address").val(response.address);
                $("#edit_primary_contact_no").val(response.primary_contact_no);
                $("#edit_secondary_contact_no").val(response.secondary_contact_no);
                $("#edit_email").val(response.email);
                $("#edit_fax").val(response.fax);

                $('#editVendor').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'vendors/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

</script>
@endsection
