@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">


@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Manage Accounts</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item">Add, Update or Delete Accounts
                        </li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All Accounts</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <button type="button" class="btn btn-info btn-min-width btn-glow btn-cons pull-right" data-toggle="modal" data-target="#createAccount">
                                            Add
                                        </button>
                                        <div class="modal fade text-left" id="createAccount" role="dialog" aria-labelledby="createAccountModal" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <label class="modal-title text-text-bold-600" id="createAccountModal">Create Account</label>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form class="add_bank_form">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="account_title">Account Title </label>
                                                                    <input type="text" class="form-control account_field" id="account_title" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="account_no">Account No.  <span class="red">*</span></label>
                                                                    <input type="text" class="form-control account_field" id="account_no" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="bank_id">Bank <span class="red">*</span></label>
                                                                    <select id="bank_id" class="form-control account_field">
                                                                        <option value="0">--Select Bank--</option>
                                                                    </select>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="bank_branch_id">Bank Branch <span class="red">*</span></label>
                                                                    <select id="bank_branch_id" class="form-control account_field">
                                                                        <option value="0">--Select Bank Branch--</option>
                                                                    </select>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="account_name">Account Name</label>
                                                                    <input type="text" class="form-control account_field" id="account_name" >
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group form-group-style">
                                                                    <label for="bank_endorsement">Bank Endorsement</label>
                                                                    <input type="text" class="form-control account_field" id="bank_endorsement" >
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                                                        <button type="button" id="submit_account" class="btn btn-success btn-glow"> Save </button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered" id="accounts">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Account no</th>
                                                    <th>Bank</th>
                                                    <th>Bank Branch</th>
                                                    <th>Name</th>
                                                    <th>Bank Endorsement</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="editAccountModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="editAccountModal">Edit Account</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="edit_account_form">
                    <div class="modal-body">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_account_title">Account Title </label>
                                    <input type="text" class="form-control edit_account_field" id="edit_account_title" >
                                    <input type="hidden" class="form-control bb_field" id="edit_id" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_account_no">Account No.  <span class="red">*</span></label>
                                    <input type="text" class="form-control edit_account_field" id="edit_account_no" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_bank_id">Bank <span class="red">*</span></label>
                                    <select id="edit_bank_id" class="form-control select2 edit_account_field">
                                        <option value="0">--Select Bank--</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_bank_branch_id">Bank Branch <span class="red">*</span></label>
                                    <select id="edit_bank_branch_id" class="form-control select2 edit_account_field">
                                        <option value="0">--Select Bank Branch--</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_account_name">Account Name</label>
                                    <input type="text" class="form-control edit_account_field" id="edit_account_name" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_bank_endorsement">Bank Endorsement</label>
                                    <input type="text" class="form-control edit_account_field" id="edit_bank_endorsement" >
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group form-group-style">
                                    <label for="edit_status">Status</label>
                                    <select id="edit_status" class="select2 form-control">
                                        <option value="0">Disable</option>
                                        <option value="1">Enable</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary btn-glow" data-dismiss="modal" >Close</button>
                    <button type="button" id="update_account" class="btn btn-success btn-glow"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/buttons.colVis.min.js') }}"></script>

<script>
    var table = $('#accounts').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('accounts.index') }}"
        },
        columns: [
            {
                data: 'account_title',
                name: 'account_title'
            },
            {
                data: 'account_no',
                name: 'account_no'
            },
            {
                data: 'bank_id',
                name: 'bank_id'
            },
            {
                data: 'bank_branch_id',
                name: 'bank_branch_id'
            },
            {
                data: 'account_name',
                name: 'account_name'
            },
            {
                data: 'bank_endorsement',
                name: 'bank_endorsement',
                visible: false
            },
            {
                data: function (data, type, dataToSet) {
                    
                    if(data.status == '1') {
                        return '<div class="badge badge-success">Enabled</div>';
                    } else {
                        return '<div class="badge badge-danger">Disabled</div>';
                    }
                    
                },
                name: 'status'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ],
        responsive: true,
        dom: 'Bfrtip',
        stateSave: true,
        buttons: [
            'colvis'
        ]
    });

    $('#submit_account').on('click',function() {
        $('#submit_account').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#submit_account').attr('disabled', 'true');

        var data = {
            'account_title' : $("#account_title").val(),
            'account_no' : $("#account_no").val(),
            'bank_id' : $("#bank_id").val(),
            'bank_branch_id' : $("#bank_branch_id").val(),
            'account_name' : $("#account_name").val(),
            'bank_endorsement' : $("#bank_endorsement").val(),
            'company_branch_id' : $("#company_branch_id").val(),
        };

        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("accounts.store") }}',
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    $('#submit_account').html('Save');
                    $('#submit_account').removeAttr('disabled');

                    $("#account_title").val('');
                    $("#account_no").val('');
                    $("#bank_id").val('');
                    $("#bank_branch_id").val('');
                    $("#bank_endorsement").val('');

                    setTimeout(function(){ 
                        $('#createAccount').modal('hide');

                        table.ajax.reload();
                    }, 1000);
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    $('#submit_account').html('Save');
                    $('#submit_account').removeAttr('disabled');
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    $('#submit_account').html('Save');
                    $('#submit_account').removeAttr('disabled');
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                $('#submit_account').html('Save');
                $('#submit_account').removeAttr('disabled');
            }
        });

    });

    $('#update_account').on('click',function() {
        $('#update_account').html('<i class="la la-circle-o-notch spinner"></i> Loading...');
        $('#update_account').attr('disabled', 'true');

        id =  $("#edit_id").val();

        var data = {
            'id' : $("#edit_id").val(),
            'account_title' : $("#edit_account_title").val(),
            'account_no' : $("#edit_account_no").val(),
            'bank_id' : $("#edit_bank_id").val(),
            'bank_branch_id' : $("#edit_bank_branch_id").val(),
            'account_name' : $("#edit_account_name").val(),
            'bank_endorsement' : $("#edit_bank_endorsement").val(),
            'company_branch_id' : $("#edit_company_branch").val(),
            'status' : $("#edit_status").val(),
        };

        $.ajax({
            type: "PUT",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'accounts/'+id,
            data: data,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {

                    toastr.success(response.text);
                    table.ajax.reload();
                    
                } else if (response.type == 'error') {

                    toastr.error(response.text);
                    
                } else {

                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }

                $('#update_account').html('Update');
                $('#update_account').removeAttr('disabled');
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
                
                $('#update_account').html('Update');
                $('#update_account').removeAttr('disabled');
            }
        });

    });

    function edit_record(id) {
        $.ajax({
            type: "GET",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'accounts/'+id+'/edit',
            dataType: "json",
            success: function(response) {

                $("#edit_id").val(response.account.id);
                $("#edit_account_title").val(response.account.account_title);
                $("#edit_account_no").val(response.account.account_no);
                $("#edit_bank_id").html(response.banks);
                $("#edit_bank_branch_id").html(response.bank_branches);
                $("#edit_company_branch").html(response.company_branches);
                $("#edit_account_name").val(response.account.account_name);
                $("#edit_bank_endorsement").val(response.account.bank_endorsement);
                $("#edit_status").val(response.account.status);

                $('#edit_status')
                    .val(response.account.status)
                    .trigger("change");

                $('#editAccount').modal('show');

            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }

    function remove_record(id) {
        $.ajax({
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'accounts/'+id,
            dataType: "json",
            success: function(response) {
                if(response.type == 'success') {
                    toastr.success(response.text);

                    table.ajax.reload();
                    
                } else if (response.type == 'error') {
                    toastr.error(response.text);
                    
                } else {
                    toastr.warning("Couldn't complete the request. Please contact management");
                    
                }
            },
            error:  function(response) {
                toastr.warning("Couldn't complete the request. Please contact management");
            }
        });
    }
    $('.select2').select2()
    $("#bank_id").select2({
        ajax: {
            url: 'bank-branches/fetch-banks',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            },
            
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        },
        width: '100%'
    });

    $("#bank_id").change(function() {
        $("#bank_branch_id").html('<option value="0">--Select Bank Branch--</option>');
    });

    $("#bank_branch_id").select2({
        ajax: {
            url: 'accounts/fetch-bank-branches',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    bank: $("#bank_id").val()
                }

                return query;
            },
            
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.bb_name + ' (' + item.bb_code + ') ',
                            id: item.id
                        }
                    })
                };
            }
        },
        width: '100%'
    });

    $("#company_branch_id").select2({
        ajax: {
            url: 'branches/fetch-company-branches',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            },
            
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.brnch_name,
                            id: item.id
                        }
                    })
                };
            }
        },
        width: '100%'
    });
</script>
@endsection
