<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{

    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('other_name')->nullable();
            $table->text('address')->nullable();
            $table->string('primary_contact_no')->nullable();
            $table->string('secondary_contact_no')->nullable();
            $table->string('email')->nullable();
            $table->string('fax')->nullable();
            $table->integer('is_deleted')->default('0');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
