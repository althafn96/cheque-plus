<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->string('shortname');
            $table->text('ho_address_1');
            $table->text('ho_address_2')->nullable();
            $table->text('ho_address_3')->nullable();
            $table->string('ho_tel_1')->nullable();
            $table->string('ho_tel_2')->nullable();
            $table->string('ho_tel_3')->nullable();
            $table->string('ho_fax_1')->nullable();
            $table->string('ho_fax_2')->nullable();
            $table->string('ho_email_1')->nullable();
            $table->string('ho_email_2')->nullable();
            $table->integer('status')->default('1');
            $table->integer('is_deleted')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
