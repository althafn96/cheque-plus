<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturnChequesTable extends Migration
{

    public function up()
    {
        Schema::create('return_cheques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('chq_id');
            $table->integer('rr_id');
            $table->date('returned_date');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('return_cheques');
    }
}
