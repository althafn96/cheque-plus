<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountColumnToChequeQueueTable extends Migration
{

    public function up()
    {
        Schema::table('cheque_queue', function (Blueprint $table) {
            $table->string('amount')->nullable();
        });
    }


    public function down()
    {
        Schema::table('cheque_queue', function (Blueprint $table) {
            $table->dropColumn('amount');
        });
    }
}
