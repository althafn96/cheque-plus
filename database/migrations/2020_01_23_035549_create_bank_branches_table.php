<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bb_code');
            $table->integer('bank_id');
            $table->string('bb_name');
            $table->string('bb_short_name')->nullable();
            $table->text('bb_address_1')->nullable();
            $table->text('bb_address_2')->nullable();
            $table->text('bb_address_3')->nullable();
            $table->string('bb_tel_1')->nullable();
            $table->string('bb_tel_2')->nullable();
            $table->string('bb_tel_3')->nullable();
            $table->string('bb_fax_1')->nullable();
            $table->string('bb_fax_2')->nullable();
            $table->string('bb_email_1')->nullable();
            $table->string('bb_email_2')->nullable();
            $table->string('bb_district')->nullable();
            $table->integer('status')->default('1');
            $table->string('bb_lcpl_region_name')->nullable();
            $table->integer('bb_suspense_status')->default('0')->nullable();
            $table->integer('bb_floating_days')->default('0')->nullable();
            $table->integer('is_deleted')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_branches');
    }
}
