<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequeStatusTable extends Migration
{
    public function up()
    {
        Schema::create('cheque_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('code');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('cheque_status');
    }
}
