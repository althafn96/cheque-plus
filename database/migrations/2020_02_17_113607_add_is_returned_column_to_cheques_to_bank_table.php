<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsReturnedColumnToChequesToBankTable extends Migration
{

    public function up()
    {
        Schema::table('cheques_to_bank', function (Blueprint $table) {
            $table->integer('is_returned')->default('0');
        });
    }

    public function down()
    {
        Schema::table('cheques_to_bank', function (Blueprint $table) {
            $table->dropColumn('is_returned');
        });
    }
}
