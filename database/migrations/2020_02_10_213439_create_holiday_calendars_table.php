<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHolidayCalendarsTable extends Migration
{

    public function up()
    {
        Schema::create('holiday_calendar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->string('allDay')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('holiday_calendar');
    }
}
