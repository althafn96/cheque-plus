<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPayeeIdToChequesTable extends Migration
{

    public function up()
    {
        Schema::table('cheques', function (Blueprint $table) {
            $table->integer('payee_id')->nullable();
        });
    }


    public function down()
    {
        Schema::table('cheques', function (Blueprint $table) {
            $table->dropColumn('payee_id');
        });
    }
}
