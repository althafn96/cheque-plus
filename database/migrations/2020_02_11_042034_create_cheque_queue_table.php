<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequeQueueTable extends Migration
{

    public function up()
    {
        Schema::create('cheque_queue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_no')->nullable();
            $table->string('cheque_no')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('bank_brnch_code')->nullable();
            $table->bigInteger('vendor_id')->nullable();
            $table->bigInteger('payee_id')->nullable();
            $table->longText('front_img')->nullable();
            $table->longText('back_img')->nullable();
            $table->date('date')->nullable();
            $table->date('received_date')->nullable();
            $table->integer('validity')->default('1');
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('cheque_queue');
    }
}
