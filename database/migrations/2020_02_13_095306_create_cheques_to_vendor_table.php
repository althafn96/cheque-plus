<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequesToVendorTable extends Migration
{

    public function up()
    {
        Schema::create('cheques_to_vendor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cheque_id');
            $table->integer('vendor_id');
            $table->date('granted_date');
            $table->integer('is_deleted')->default('0');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('cheques_to_vendor');
    }
}
