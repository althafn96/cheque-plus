<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsSavedColumnToChequeQueueTable extends Migration
{

    public function up()
    {
        Schema::table('cheque_queue', function (Blueprint $table) {
            $table->integer('is_saved')->default('0');
            $table->date('scanned_date')->nullable();
        });
    }


    public function down()
    {
        Schema::table('cheque_queue', function (Blueprint $table) {
            $table->dropColumn('is_saved');
            $table->dropColumn('scanned_date');
        });
    }
}
