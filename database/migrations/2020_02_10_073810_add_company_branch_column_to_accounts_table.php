<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyBranchColumnToAccountsTable extends Migration
{

    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->bigInteger('company_branch')->nullable();
        });
    }


    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('company_branch');
        });
    }
}
