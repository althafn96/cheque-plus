<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountToChequesTable extends Migration
{

    public function up()
    {
        Schema::table('cheques', function (Blueprint $table) {
            $table->string('amount')->nullable();
        });
    }


    public function down()
    {
        Schema::table('cheques', function (Blueprint $table) {
            $table->dropColumn('amount');
        });
    }
}
