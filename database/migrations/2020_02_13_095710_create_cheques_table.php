<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequesTable extends Migration
{

    public function up()
    {
        Schema::create('cheques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('status_id');
            $table->integer('vendor_id');
            $table->string('cheque_no');
            $table->string('account_no');
            $table->integer('bank_id');
            $table->integer('bank_branch_id');
            $table->date('deposit_date');
            $table->date('received_date');
            $table->longText('image_front');
            $table->longText('image_back');
            $table->integer('is_hold')->default('0');
            $table->integer('is_deleted')->default('0');
        });
    }

    public function down()
    {
        Schema::dropIfExists('cheques');
    }
}
