<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class ChequesInHandController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'Cheques in Hand';
        $sys_date = Controller::get_system_date();
        $cheques = DB::table('cheques')->get();

        $branches = DB::table('branches')
            ->select('id', 'brnch_name')
            ->where('status', '1')
            ->get();

        $vendors = DB::table('vendors')
            ->select('id', 'name')
            ->where('is_deleted', '0')
            ->get();

        if ($request->ajax()) {
            if ($request->payee == '0' && $request->vendor == '0') {
                $cheques = DB::table('cheques')->where('status_id', '3')->where('is_deleted', '0')->where('is_hold', '0')->get();
            } else {
                if ($request->payee == '0') {
                    $cheques = DB::table('cheques')
                        ->where('status_id', '3')
                        ->where('vendor_id', $request->vendor)
                        ->where('is_deleted', '0')
                        ->where('is_hold', '0')
                        ->get();
                } else if ($request->vendor == '0') {
                    $cheques = DB::table('cheques')
                        ->where('status_id', '3')
                        ->where('payee_id', $request->payee)
                        ->where('is_deleted', '0')
                        ->where('is_hold', '0')
                        ->get();
                } else {
                    $cheques = DB::table('cheques')
                        ->where('status_id', '3')
                        ->where('vendor_id', $request->vendor)
                        ->where('payee_id', $request->payee)
                        ->where('is_deleted', '0')
                        ->where('is_hold', '0')
                        ->get();
                }
            }

            return DataTables::of($cheques)
                ->addColumn('status', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('vendor_id', function ($cheque) {
                    $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

                    return $vendor->name;
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('bank_branch', function ($cheque) {
                    $bank_branch =  DB::table('banks as b')
                        ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
                        ->select('b.name', 'bb.bb_name')
                        ->where('b.id', $cheque->bank_id)
                        ->where('bb.id', $cheque->bank_branch_id)
                        ->first();

                    return $bank_branch->name . ' (' . $bank_branch->bb_name . ')';
                })
                ->addColumn('res', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('action', function ($cheque) {

                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="show_cheque(' . $cheque->id . ')">Show</button>
                                        <button class="dropdown-item" onclick="hold_cheque(' . $cheque->id . ')">Hold</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make('true');
        }

        return view('cheques.hand', compact('pageTitle', 'sys_date', 'branches', 'vendors'));
    }

    public function hold_cheque(Request $request)
    {
        $chq_id = $request->chq_id;

        DB::table('cheques')->where('id', $chq_id)->update(['is_hold' => '1']);

        return json_encode(array('type' => 'success', 'text' => 'Cheque moved to Cheques on Hold'));
    }

    public function on_hold_cheques(Request $request)
    {
        if ($request->ajax()) {
            $cheques = DB::table('cheques')->where('status_id', '3')->where('is_deleted', '0')->where('is_hold', '1')->get();
            return DataTables::of($cheques)
                ->addColumn('status', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('vendor_id', function ($cheque) {
                    $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

                    return $vendor->name;
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('res', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('action', function ($cheque) {

                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="show_cheque(' . $cheque->id . ')">Show</button>
                                        <button class="dropdown-item" onclick="release_cheque(' . $cheque->id . ')">Release</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make('true');
        }

        return view('cheques.hand', compact('pageTitle', 'sys_date'));
    }

    public function release_cheque(Request $request)
    {
        $chq_id = $request->chq_id;

        DB::table('cheques')->where('id', $chq_id)->update(['is_hold' => '0']);

        return json_encode(array('type' => 'success', 'text' => 'Cheque released successfully'));
    }

    public function release_selected_cheques(Request $request)
    {
        if ($request->selected_ids == '') {
            return json_encode(array('type' => 'error', 'text' => 'Please select a cheque/cheques that has to be released'));
        }
        $ids_arr = explode(',', $request->selected_ids);
        $count = 0;
        foreach ($ids_arr as $id) {
            if ($id != '') {
                $returnValue = DB::table('cheques')->where('id', $id)->update(['is_hold' => '0']);

                if ($returnValue != 0) {
                    $count++;
                }
            }
        }

        if ($count == 1) {
            $text = '1 cheque released successfully';
        } else if ($count == 0) {
            $text = '0 cheques released';
        } else {
            $text = $count . ' cheques released successfully';
        }

        return json_encode(array('type' => 'success', 'text' => $text));
    }

    public function hold_selected_cheques(Request $request)
    {
        if ($request->selected_ids == '') {
            return json_encode(array('type' => 'error', 'text' => 'Please select a cheque/cheques that has to be on hold'));
        }
        $ids_arr = explode(',', $request->selected_ids);
        $count = 0;
        foreach ($ids_arr as $id) {
            if ($id != '') {
                $returnValue = DB::table('cheques')->where('id', $id)->update(['is_hold' => '1']);

                if ($returnValue != 0) {
                    $count++;
                }
            }
        }

        if ($count == 1) {
            $text = '1 cheque moved to cheques on hold successfully';
        } else if ($count == 0) {
            $text = '0 cheques moved to cheques on hold';
        } else {
            $text = $count . ' cheques moved to cheques on hold successfully';
        }

        return json_encode(array('type' => 'success', 'text' => $text));
    }

    public function show_cheque_image(Request $request)
    {
        $id = $request->id;

        $cheque = DB::table('cheques')->where('id', $id)->first();

        $image_front = explode("\\", $cheque->image_front);

        $image_behind = explode("\\", $cheque->image_back);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        $cheque_no = $cheque->cheque_no;

        return json_encode(array('type' => 'success', 'image_front' => $image_front, 'image_behind' => $image_behind, 'cheque_no' => $cheque_no));
    }
}
