<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;
use DataTables;

class BanksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {

        $pageTitle = 'Banks';
        $banks = Bank::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();

        if ($request->ajax()) {
            $banks = Bank::latest()->where('is_deleted', '0')->get();
            return DataTables::of($banks)
                ->addColumn('action', function ($banks) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $banks->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $banks->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('banks.index', compact('pageTitle', 'banks', 'sys_date'));
    }


    public function create()
    {
    }


    public function store(Request $request)
    {

        if ($request->code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank code cannot be empty'));
            die($output);
        }

        if ($request->name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank name cannot be empty'));
            die($output);
        }

        if ($request->shortname == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank shortname cannot be empty'));
            die($output);
        }

        if ($request->ho_address_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Head office address cannot be empty (atleast 1 field needs to be filled)'));
            die($output);
        }

        if ($request->ho_tel_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Head office telephone number cannot be empty'));
            die($output);
        }

        if ($request->ho_email_1 != '' && !filter_var($request->ho_email_1, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        if ($request->ho_email_2 != '' && !filter_var($request->ho_email_2, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        $bank               = new Bank;

        $bank->code         = $request->code;
        $bank->name         = $request->name;
        $bank->shortname    = $request->shortname;
        $bank->ho_address_1 = $request->ho_address_1;
        $bank->ho_address_2 = $request->ho_address_2;
        $bank->ho_address_3 = $request->ho_address_3;
        $bank->ho_tel_1     = $request->ho_tel_1;
        $bank->ho_tel_2     = $request->ho_tel_2;
        $bank->ho_tel_3     = $request->ho_tel_3;
        $bank->ho_fax_1     = $request->ho_fax_1;
        $bank->ho_fax_2     = $request->ho_fax_2;
        $bank->ho_email_1   = $request->ho_email_1;
        $bank->ho_email_2   = $request->ho_email_2;

        // $bank->save();
        if ($bank->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function show(Bank $bank)
    {
    }


    public function edit(Bank $bank)
    {
        return $bank;
    }


    public function update(Request $request, Bank $bank)
    {
        if ($request->code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank code cannot be empty'));
            die($output);
        }

        if ($request->name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank name cannot be empty'));
            die($output);
        }

        if ($request->shortname == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank shortname cannot be empty'));
            die($output);
        }

        if ($request->ho_address_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Head office address cannot be empty (atleast 1 field needs to be filled)'));
            die($output);
        }

        if ($request->ho_tel_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Head office telephone number cannot be empty'));
            die($output);
        }

        if ($request->ho_email_1 != '' && !filter_var($request->ho_email_1, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        if ($request->ho_email_2 != '' && !filter_var($request->ho_email_2, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        $bank->code         = $request->code;
        $bank->name         = $request->name;
        $bank->shortname    = $request->shortname;
        $bank->ho_address_1 = $request->ho_address_1;
        $bank->ho_address_2 = $request->ho_address_2;
        $bank->ho_address_3 = $request->ho_address_3;
        $bank->ho_tel_1     = $request->ho_tel_1;
        $bank->ho_tel_2     = $request->ho_tel_2;
        $bank->ho_tel_3     = $request->ho_tel_3;
        $bank->ho_fax_1     = $request->ho_fax_1;
        $bank->ho_fax_2     = $request->ho_fax_2;
        $bank->ho_email_1   = $request->ho_email_1;
        $bank->ho_email_2   = $request->ho_email_2;
        $bank->status       = $request->status;

        // $bank->save();
        if ($bank->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank updated successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function destroy(Bank $bank)
    {

        $bank->is_deleted = '1';
        $bank->status     = '0';

        if ($bank->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }
}
