<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class GrantChequesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function grant_cheques(Request $request)
    {
        $sys_date = Controller::get_system_date();


        if ($request->selected_ids == '') {
            return 'Please select a cheque/cheques to grant to ' . $request->type;
        }
        $ids_arr = explode(',', $request->selected_ids);
        $cheques_arr = array();

        foreach ($ids_arr as $id) {
            if ($id != '') {
                $cheque_row = DB::table('cheques')->where('id', $id)->first();

                array_push($cheques_arr, $cheque_row);
            }
        }

        if ($request->type == 'bank') {
            $bnk_diff = false;
            $row_id = $cheques_arr[0]->bank_id;
        } else if ($request->type == 'vendor') {
            $bnk_diff = false;
            $row_id = $cheques_arr[0]->vendor_id;

            foreach ($cheques_arr as $ca) {
                if ($ca->vendor_id != $row_id) {
                    return 'multiple vendors';
                }
            }
        }

        $ciq_select_list = '';
        $total_in_queue = 0;

        $total_amount = 0;

        foreach ($cheques_arr as $ca) {
            if ($ca->id == $cheques_arr[0]->id) {
                $ciq_select_list .= '<option selected value="' . $ca->id . '">Cheque No. ' . $ca->cheque_no . '</option>';
            } else {
                $ciq_select_list .= '<option value="' . $ca->id . '">Cheque No. ' . $ca->cheque_no . '</option>';
            }

            $total_in_queue++;
            $total_amount += $ca->amount;
        }

        $image_front = explode("\\", $cheques_arr[0]->image_front);

        $image_behind = explode("\\", $cheques_arr[0]->image_back);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        $bank_branch =  DB::table('banks as b')
            ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
            ->select('b.name', 'bb.bb_name')
            ->where('b.id', $cheques_arr[0]->bank_id)
            ->where('bb.id', $cheques_arr[0]->bank_branch_id)
            ->first();

        $vendor =  DB::table('vendors')->select('name')->where('id', $cheques_arr[0]->vendor_id)->first();

        $select_list = '';
        $label = '';
        if ($request->type == 'bank') {
            $bank_accounts = DB::table('accounts as acc')
                ->join('bank_branches as bb', 'acc.bank_branch_id', '=', 'bb.id')
                ->select('acc.id', 'account_no', 'bb_name')
                ->where('acc.status', '1')
                ->get();

            foreach ($bank_accounts as $ba) {
                $select_list .= '<option value="' . $ba->id . '">' . $ba->account_no . ' ( ' . $ba->bb_name . ' )' . '</option>';
            }

            $label = 'Bank Account';
        } else {
            $vendors = DB::table('vendors')
                ->where('id', $row_id)
                ->get();

            foreach ($vendors as $vendor) {
                $select_list .= '<option value="' . $vendor->id . '">' . $vendor->name . ' ( ' . $vendor->code . ' )' . '</option>';
            }

            $label = 'Vendor';
        }


        $output = '
                    <div class="row p-1">
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="cheque_no_on_grant_queue">Cheque No.</label>
                                <select id="cheque_no_on_grant_queue" class="form-control select2">
                                    ' . $ciq_select_list . '
                                </select>
                            </fieldset>
                            <ul class="list-group grant_queue" id="chq_list_details">
                                <li class="list-group-item"><strong>Branch:</strong> ' . $bank_branch->name . ' (' . $bank_branch->bb_name . ')' . ' </li>
                                <li class="list-group-item"><strong>Vendor:  </strong>' . $vendor->name . '</li>
                                <li class="list-group-item"><strong>Deposit Date:  </strong>' . $cheques_arr[0]->deposit_date . '</li>
                                <li class="list-group-item"><strong>Account No.:  </strong>' . $cheques_arr[0]->account_no . '</li>
                                <li class="list-group-item"><strong>Amount:  </strong> Rs. ' . $cheques_arr[0]->amount . '</li>
                            </ul>

                            <ul class="mt-2 list-unstyled">
                                <li>Total Number of Cheques</li>
                                <li class="lead text-bold-800">' . $total_in_queue . '</li>
                            </ul>
                            <p>AMOUNT</p>
                            <h2 class="pb-1">Rs. ' . $total_amount . '</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="form-group form-group-style">
                                        <label for="grant_date">Grant Date <span class="red">*</span></label>
                                        <input value="' . $sys_date . '" autofocus type="date" class="form-control" id="grant_date">
                                    </fieldset> 
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group form-group-style">
                                        <label for="grant_type_id">' . $label . ' <span class="red">*</span></label>
                                        <select id="grant_type_id" class="form-control select2">
                                            ' . $select_list . '
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <button dataType="' . $request->type . '" dataList="' . $request->selected_ids . '" id="submit_granting_cheques" type="button" class="btn btn-info btn-glow float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 grant_queue" id="chq_images">
                            <img src="' . asset('chq_source/' . $image_front) . '" style="width:100%;height:auto;margin-bottom:10px;">
                            <img src="' . asset('chq_source/' . $image_behind) . '" style="width:100%;height:auto;margin-bottom:10px;">
                        </div>
                    </div>

                    <script> $(".select2").select2(); </script>
                ';

        return $output;
    }

    public function load_selected_cheque_in_grant_queue(Request $request)
    {
        $id = $request->id;

        $cheque = DB::table('cheques')->where('id', $id)->first();

        $bank_branch =  DB::table('banks as b')
            ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
            ->select('b.name', 'bb.bb_name')
            ->where('b.id', $cheque->bank_id)
            ->where('bb.id', $cheque->bank_branch_id)
            ->first();

        $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

        $chq_list_details = '<ul class="list-group grant_queue" id="chq_list_details">
                                <li class="list-group-item"><strong>Bank:</strong> ' . $bank_branch->name . ' (' . $bank_branch->bb_name . ')' . ' </li>
                                <li class="list-group-item"><strong>Vendor:  </strong>' . $vendor->name . '</li>
                                <li class="list-group-item"><strong>Deposit Date:  </strong>' . $cheque->deposit_date . '</li>
                                <li class="list-group-item"><strong>Account No.:  </strong>' . $cheque->account_no . '</li>
                                <li class="list-group-item"><strong>Amount:  </strong> Rs. ' . $cheque->amount . '</li>
                            </ul>';

        $image_front = explode("\\", $cheque->image_front);

        $image_behind = explode("\\", $cheque->image_back);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        $chq_images = '
                        <img src="' . asset('chq_source/' . $image_front) . '" style="width:100%;height:auto;margin-bottom:10px;">
                        <img src="' . asset('chq_source/' . $image_behind) . '" style="width:100%;height:auto;margin-bottom:10px;">
                    ';
        return json_encode(array('type' => 'success', 'chq_list_details' => $chq_list_details, 'chq_images' => $chq_images));
    }

    public function submit_grant_cheques(Request $request)
    {
        $selected_ids = $request->selected_ids;
        $type = $request->type;
        $grant_date = $request->grant_date;
        $grant_type_id = $request->grant_type_id;

        if ($grant_date == '') {
            return json_encode(array('type' => 'error', 'text' => 'Please enter the grant date'));
        }
        if ($grant_type_id == '') {
            if ($request->type == 'bank') {
                return json_encode(array('type' => 'error', 'text' => 'Please select an account no.'));
            } else if ($request->type == 'vendor') {
                return json_encode(array('type' => 'error', 'text' => 'Please select a vendor'));
            }
        }

        $ids_arr = explode(',', $selected_ids);

        foreach ($ids_arr as $id) {
            if ($id != '') {
                if ($request->type == 'bank') {
                    DB::table('cheques')->where('id', $id)->update(['status_id' => '1']);

                    DB::table('cheques_to_bank')->insert([
                        'cheque_id' => $id,
                        'account_id' => $grant_type_id,
                        'granted_date' => $grant_date,
                        'created_at' => Carbon::now()->setTimezone('Asia/Colombo')
                    ]);
                } else if ($request->type == 'vendor') {
                    DB::table('cheques')->where('id', $id)->update(['status_id' => '2']);

                    DB::table('cheques_to_vendor')->insert([
                        'cheque_id' => $id,
                        'vendor_id' => $grant_type_id,
                        'granted_date' => $grant_date,
                        'created_at' => Carbon::now()->setTimezone('Asia/Colombo')
                    ]);
                }
            }
        }

        return json_encode(array('type' => 'success', 'text' => 'Cheques granted to ' . $request->type));
    }
}
