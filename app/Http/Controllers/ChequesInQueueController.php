<?php

namespace App\Http\Controllers;

use App\Vendor;
use Carbon\Carbon;
use League\Csv\Reader;
use League\Csv\Writer;
use League\Csv\Statement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChequesInQueueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pageTitle = 'Cheques in Queue';
        $sys_date = Controller::get_system_date();

        $csv = Reader::createFromPath('chq_source/chq.csv', 'r');
        $stmt = (new Statement());

        $records = $stmt->process($csv);

        $i = 0;
        foreach ($records as $record) {

            $image = $record[1];

            try {
                $cheque_details_arr = explode("<", $record[2]);
                $cheque_no = $cheque_details_arr[1];

                $bank_branch_account_details_arr = explode(":", $cheque_details_arr[2]);

                $bank_brnch_code = explode("=", $bank_branch_account_details_arr[0]);

                $bank_code = trim($bank_brnch_code[0]);
                $branch_code = $bank_brnch_code[1];

                $account_no = $bank_branch_account_details_arr[1];
            } catch (\Exception $e) {
                $cheque_no = $record[2];
                $account_no = $record[2];
                $bank_code = $record[2];
                $branch_code = $record[2];
            }

            if ($i % 2 == 0) {

                $update = DB::table('cheque_queue')->where([
                    ['cheque_no', '=', $cheque_no],
                    ['account_no', '=', $account_no],
                    ['bank_code', '=', $bank_code],
                    ['bank_brnch_code', '=', $branch_code],
                    ['is_saved', '=', '0']
                ])->update([
                    'front_img' => $image
                ]);

                if ($update == 0) {
                    DB::table('cheque_queue')->insert([
                        'cheque_no' => $cheque_no,
                        'account_no' => $account_no,
                        'bank_code' => $bank_code,
                        'bank_brnch_code' => $branch_code,
                        'front_img' => $image,
                        'scanned_date' => Carbon::now()->setTimezone('Asia/Colombo')->toDateString()
                    ]);
                }
            } else {
                $update = DB::table('cheque_queue')->where([
                    ['cheque_no', '=', $cheque_no],
                    ['account_no', '=', $account_no],
                    ['bank_code', '=', $bank_code],
                    ['bank_brnch_code', '=', $branch_code],
                    ['is_saved', '=', '0']
                ])->update([
                    'back_img' => $image
                ]);
            }

            $i++;
        }

        $writer = Writer::createFromPath('chq_source/chq.csv', 'w+');
        $writer->insertOne([]);
        return view('cheques.queue', compact('pageTitle', 'sys_date'));
    }

    public function load_to_queue()
    {
        $count = DB::table('cheque_queue')->where('is_saved', '0')->count();

        $i = 1;
        $id = 1;

        if ($count == '0') {
            return '<div style="text-align: center">No Cheques in Queue</div>';
        }

        $cheque_in_queue = DB::table('cheque_queue')->get();
        // dd($cheque_in_queue);

        foreach ($cheque_in_queue as $ciq) {
            if (
                $ciq->amount == 0 ||
                $ciq->amount == null ||
                $ciq->vendor_id == 0 ||
                $ciq->vendor_id == null ||
                $ciq->payee_id == 0 ||
                $ciq->payee_id == null ||
                $ciq->date == null ||
                $ciq->received_date == null
            ) {
                $id = $ciq->id;
                break;
            }
            $i++;
        }

        if ($i > $count) {
            $i = 1;
        }

        $output = $this->cheque_queue($count, $i, $id, 'null');

        return $output;
    }

    public function load_next_to_queue(Request $request)
    {

        $id = $request->id;
        $curr_cheque_count = $request->count;

        $count = DB::table('cheque_queue')->where('is_saved', '0')->count();

        $output = $this->cheque_queue($count, $curr_cheque_count + 1, $id, 'next');

        return $output;
    }

    public function load_prev_to_queue(Request $request)
    {

        $id = $request->id;
        $curr_cheque_count = $request->count;

        $count = DB::table('cheque_queue')->where('is_saved', '0')->count();

        $output = $this->cheque_queue($count, $curr_cheque_count - 1, $id, 'prev');

        return $output;
    }

    public function load_cheque_from_select(Request $request)
    {

        $id = $request->id;

        $count = DB::table('cheque_queue')->where('is_saved', '0')->count();

        $i = 1;
        $curr_cheque_count = 0;

        $cheques_in_queue = DB::table('cheque_queue')->get();

        foreach ($cheques_in_queue as $ciq) {
            if ($ciq->id == $id) {
                $curr_cheque_count = $i;
            }
            $i++;
        }

        $output = $this->cheque_queue($count, $curr_cheque_count, $id, 'null');

        return $output;
    }

    public function cheque_queue($count, $i, $id, $type)
    {
        if ($id == 0) {
            $ciq = DB::table('cheque_queue')->where('is_saved', '0')->first();
        } else {
            if ($type == 'null') {
                $ciq = DB::table('cheque_queue')->where('id', '=', $id)->first();
            } else if ($type == 'next') {
                $ciq = DB::table('cheque_queue')->where('id', '>', $id)->first();
            } else if ($type == 'prev') {
                $ciq = DB::table('cheque_queue')->where('id', '<', $id)->orderBy('id', 'desc')->first();
            }
        }
        $bank = DB::table('banks')->where('code', $ciq->bank_code)->first();

        if ($bank == null || $bank == '') {
            $bank_name = '
                            <div class="col-md-3">
                                <span style="font-size: 10px" class="red">couldnt read bank code. please enter manually</span> 
                                <fieldset class="form-group form-group-style">
                                    <label for="bank_code">Bank Code <span class="red">*</span></label>
                                    <input autofocus type="text" class="form-control ciq_field" id="bank_code" >
                                </fieldset>    
                            </div>
                        ';

            $branch_name = '
                        <div class="col-md-3">
                            <span style="font-size: 10px" class="red">couldnt read bank branch code. please enter manually</span> 
                            <fieldset class="form-group form-group-style">
                                <label for="bank_brnch_code">Bank Branch Code <span class="red">*</span></label>
                                <input autofocus type="text" class="form-control ciq_field" id="bank_brnch_code" >
                            </fieldset>    
                        </div>
                    ';
        } else {
            $bank_name = '<div class="col-md-3 mt-2 mb-2">
                            <strong>Bank: </strong>  ' . $bank->name . '
                          </div>';

            $branch = DB::table('bank_branches')->where('bank_id', $bank->id)->where('bb_code', $ciq->bank_brnch_code)->first();

            if ($branch == null || $branch == '') {
                $branch_name = '
                                <div class="col-md-3">
                                    <span style="font-size: 10px" class="red">couldnt read bank branch code. please enter manually</span> 
                                    <fieldset class="form-group form-group-style">
                                        <label for="bank_brnch_code">Bank Branch Code <span class="red">*</span></label>
                                        <input autofocus type="text" class="form-control ciq_field" id="bank_brnch_code" >
                                    </fieldset>    
                                </div>
                            ';
            } else {
                $branch_name = '<div class="col-md-3 mt-2 mb-2">
                                <strong>Branch: </strong>  ' . $branch->bb_name . '
                            </div>';
            }
        }

        $account_no = $ciq->account_no;

        if (strpos($ciq->account_no, '@') !== false || $ciq->account_no == '') {
            $account_no = '
                            <div class="col-md-3">
                                <span style="font-size: 10px" class="red">couldnt read account no. please enter manually</span> 
                                <fieldset class="form-group form-group-style">
                                    <label for="account_no">Account No <span class="red">*</span></label>
                                    <input autofocus type="text" class="form-control ciq_field" id="account_no" >
                                </fieldset>    
                            </div>
                        ';
        } else {
            $account_no = '<div class="col-md-3 mt-2 mb-2">
                            <strong>Account No: </strong>  ' . $ciq->account_no . '
                          </div>';
        }

        $cheque_no = $ciq->cheque_no;

        if (strpos($ciq->cheque_no, '@') !== false || $ciq->cheque_no == '') {
            $cheque_no = '
                            <div class="col-md-3">
                                <span style="font-size: 10px" class="red">couldnt read cheque no. please enter manually</span> 
                                <fieldset class="form-group form-group-style">
                                    <label for="cheque_no">Cheque No <span class="red">*</span></label>
                                    <input autofocus type="text" class="form-control ciq_field" id="cheque_no" >
                                </fieldset>    
                            </div>
                        ';
        } else {
            $cheque_no = '<div class="col-md-3 mt-2 mb-2">
                            <strong>Cheque No: </strong>  ' . $ciq->cheque_no . '
                          </div>';
        }

        $sys_date = Controller::get_system_date();

        $output = '';

        $vendors = DB::table('vendors')->where('is_deleted', '0')->get();

        if ($ciq->vendor_id == '') {
            $vendors_list = '
                            <select id="vendor_id" class="form-control select2 ciq_field">
                                <option value="0">--Select Vendor--</option>';
        } else {

            $vendors_list = '
                            <select disabled id="vendor_id" class="form-control select2 ciq_field">
                                <option value="0">--Select Vendor--</option>';
        }

        foreach ($vendors as $vendor) {
            if ($vendor->id == $ciq->vendor_id) {
                $vendors_list .= '<option selected value="' . $vendor->id . '">' . $vendor->name . '</option>';
            } else {
                $vendors_list .= '<option value="' . $vendor->id . '">' . $vendor->name . '</option>';
            }
        }


        $vendors_list .= '</select>';

        $branches = DB::table('branches')->where('status', '1')->get();

        if ($ciq->payee_id == '') {
            $branches_list = '
                                <select id="payee_id" class="form-control select2 ciq_field">
                                    <option value="0">--Select Payee--</option>';
        } else {
            $branches_list = '
                                <select disabled id="payee_id" class="form-control select2 ciq_field">
                                    <option value="0">--Select Payee--</option>';
        }

        foreach ($branches as $branch) {
            if ($branch->id == $ciq->payee_id) {
                $branches_list .= '<option selected value="' . $branch->id . '">' . $branch->brnch_name . '</option>';
            } else {
                $branches_list .= '<option value="' . $branch->id . '">' . $branch->brnch_name . '</option>';
            }
        }
        $branches_list .= '</select>';

        $all_cheques_in_queue = DB::table('cheque_queue')->get();

        $all_cheques_in_queue_list = '';

        foreach ($all_cheques_in_queue as $aciq) {
            $all_cheques_in_queue_list .= '<option ';
            if (
                $aciq->amount == 0 ||
                $aciq->amount == null ||
                $aciq->vendor_id == 0 ||
                $aciq->vendor_id == null ||
                $aciq->payee_id == 0 ||
                $aciq->payee_id == null ||
                $aciq->date == null ||
                $aciq->received_date == null
            ) {
                if (
                    $aciq->cheque_no == '' ||
                    strpos($aciq->cheque_no, '@') !== false ||
                    $aciq->account_no == '' ||
                    strpos($aciq->account_no, '@') !== false ||
                    $aciq->bank_code == '' ||
                    strpos($aciq->bank_code, '@') !== false ||
                    $aciq->bank_brnch_code == '' ||
                    strpos($aciq->bank_brnch_code, '@') !== false
                ) {
                    $all_cheques_in_queue_list .= ' data-icon="warning" ';

                    if (
                        $aciq->cheque_no == '' ||
                        strpos($aciq->cheque_no, '@') !== false
                    ) {
                        $aciq->cheque_no = 'read error';
                    }
                }
            } else {
                if (
                    $aciq->cheque_no == '' ||
                    strpos($aciq->cheque_no, '@') !== false ||
                    $aciq->account_no == '' ||
                    strpos($aciq->account_no, '@') !== false ||
                    $aciq->bank_code == '' ||
                    strpos($aciq->bank_code, '@') !== false ||
                    $aciq->bank_brnch_code == '' ||
                    strpos($aciq->bank_brnch_code, '@') !== false
                ) {
                    $all_cheques_in_queue_list .= ' data-icon="warning" ';

                    if (
                        $aciq->cheque_no == '' ||
                        strpos($aciq->cheque_no, '@') !== false
                    ) {
                        $aciq->cheque_no = 'read error';
                    }
                } else {

                    $all_cheques_in_queue_list .= ' data-icon="check" ';
                }
            }

            if ($aciq->id == $ciq->id) {
                $all_cheques_in_queue_list .= 'selected value="' . $aciq->id . '">Cheque No: ' . $aciq->cheque_no;
            } else {
                $all_cheques_in_queue_list .= 'value="' . $aciq->id . '">Cheque No: ' . $aciq->cheque_no;
            }

            $all_cheques_in_queue_list .=  '</option>';
        }

        $image_front = explode("\\", $ciq->front_img);

        $image_behind = explode("\\", $ciq->back_img);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        return $this->get_queue_content($i, $count, $sys_date, $vendors_list, $branches_list, $all_cheques_in_queue_list, $bank_name, $branch_name, $ciq->id, $account_no, $cheque_no, $image_front, $image_behind);
    }

    public function get_queue_content($i, $count, $sys_date, $vendors_list, $branches_list, $all_cheques_in_queue_list, $bank_name, $branch_name, $id, $account_no, $cheque_no, $image_front, $image_behind)
    {
        $ciq = DB::table('cheque_queue')->where('id', $id)->first();

        if ($i == 1) {
            if ($count == 1) {
                $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" style="display: none" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" style="display: none" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
            } else {
                $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" style="display: none" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
            }
        } else {
            if ($count == 1) {
                if ($i == $count) {
                    $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                    $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" style="display: none" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
                } else {
                    $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                    $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
                }
            } else {
                if ($i == $count) {
                    $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                    $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" style="display: none" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
                } else {
                    $prev_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="prev_cheque" type="button" value="Previous Cheque" class="btn btn-warning btn-glow">';
                    $next_button = '<input data-ID="' . $id . '" data-Count="' . $i . '" id="next_cheque" type="button" value="Next Cheque" class="btn btn-success btn-glow">';
                }
            }
        }

        return '
                <div class="row">
                <div class="col-md-12 mb-2">
                    <h5>Cheque Count : ' . $i . ' / ' . $count . '</h5>
                </div>
                ' . $bank_name . '
                ' . $branch_name . '
                ' . $cheque_no . '
                ' . $account_no . ' 
                <div class="col-md-2">
                    <fieldset class="form-group form-group-style">
                        <label for="received_date">Received date <span class="red">*</span></label>
                        <input disabled type="date" value="' . $sys_date . '" class="form-control ciq_field" id="received_date" >
                        <input type="hidden" value="' . $ciq->id . '" class="form-control ciq_field" id="ciq_id" >
                    </fieldset>    
                </div>
                <div class="col-md-2">
                    <fieldset class="form-group form-group-style">
                        <label for="deposit_date">Deposit date <span class="red">*</span></label>
                        <input autofocus type="date" class="form-control ciq_field" id="date" value="' . $ciq->date . '" >
                    </fieldset>    
                </div>
                <div class="col-md-2">
                    <fieldset class="form-group form-group-style">
                        <label for="amount">Amount <span class="red">*</span></label>
                        <input type="number" class="form-control ciq_field" id="amount" value="' . $ciq->amount . '" >
                    </fieldset>    
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group form-group-style">
                        <label for="vendor_id">Vendor <span class="red">*</span></label>
                        ' . $vendors_list . '
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group form-group-style">
                        <label for="payee_id">Payee <span class="red">*</span></label>
                        ' . $branches_list . '
                    </fieldset>
                </div>
                <div class="col-md-9">
                    ' . $next_button . '
                    ' . $prev_button . '
                    
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group form-group-style">
                        <select id="chq_q_list" class="form-control select2-icons">
                            ' . $all_cheques_in_queue_list . '
                        </select>
                    </fieldset>
                </div>
                <div class="col-md-3 offset-md-9 mb-2">
                    <input data-ID="' . $id . '" id="remove_cheque" type="button" value="Remove from queue" class="btn btn-danger btn-glow float-right">
                </div>

                <div class="col-md-6 mt-1">
                    <img src="' . asset('chq_source/' . $image_front) . '" style="width:100%;height:auto;margin-bottom:10px;">
                </div>
                <div class="col-md-6 mt-1">
                    <img src="' . asset('chq_source/' . $image_behind) . '" style="width:100%;height:auto;margin-bottom:10px;">
                </div>
            </div>
            <script>
                $(".select2").select2();
                $(".select2-icons").select2({
                    templateResult: iconFormat,
                    templateSelection: iconFormat,
                    escapeMarkup: function(es) { return es; }
                });
            
                // Format icon
                function iconFormat(icon) {
                    var originalOption = icon.element;
                    if (!icon.id) { return icon.text; }
                    var $icon = "<i class=\'la la-" + $(icon.element).data(\'icon\') + "\'></i>" + icon.text;
            
                    return $icon;
                }
            </script>
                ';
    }

    public function save_ciq_field_value(Request $request)
    {
        $sys_date = Controller::get_system_date();

        DB::table('cheque_queue')->where('id', $request->id)->update([
            $request->field => $request->value,
            'received_date' => $sys_date
        ]);

        $all_cheques_in_queue = DB::table('cheque_queue')->get();

        $all_cheques_in_queue_list = '';

        foreach ($all_cheques_in_queue as $aciq) {
            $all_cheques_in_queue_list .= '<option ';
            if (
                $aciq->amount == 0 ||
                $aciq->amount == null ||
                $aciq->vendor_id == 0 ||
                $aciq->vendor_id == null ||
                $aciq->payee_id == 0 ||
                $aciq->payee_id == null ||
                $aciq->date == null ||
                $aciq->received_date == null
            ) {
                if (
                    $aciq->cheque_no == '' ||
                    strpos($aciq->cheque_no, '@') !== false ||
                    $aciq->account_no == '' ||
                    strpos($aciq->account_no, '@') !== false ||
                    $aciq->bank_code == '' ||
                    strpos($aciq->bank_code, '@') !== false ||
                    $aciq->bank_brnch_code == '' ||
                    strpos($aciq->bank_brnch_code, '@') !== false
                ) {
                    $all_cheques_in_queue_list .= ' data-icon="warning" ';

                    if (
                        $aciq->cheque_no == '' ||
                        strpos($aciq->cheque_no, '@') !== false
                    ) {
                        $aciq->cheque_no = 'read error';
                    }
                }
            } else {
                if (
                    $aciq->cheque_no == '' ||
                    strpos($aciq->cheque_no, '@') !== false ||
                    $aciq->account_no == '' ||
                    strpos($aciq->account_no, '@') !== false ||
                    $aciq->bank_code == '' ||
                    strpos($aciq->bank_code, '@') !== false ||
                    $aciq->bank_brnch_code == '' ||
                    strpos($aciq->bank_brnch_code, '@') !== false
                ) {
                    $all_cheques_in_queue_list .= ' data-icon="warning" ';

                    if (
                        $aciq->cheque_no == '' ||
                        strpos($aciq->cheque_no, '@') !== false
                    ) {
                        $aciq->cheque_no = 'read error';
                    }
                } else {

                    $all_cheques_in_queue_list .= ' data-icon="check" ';
                }
            }

            if ($aciq->id == $request->id) {
                $all_cheques_in_queue_list .= 'selected value="' . $aciq->id . '">Cheque No: ' . $aciq->cheque_no;
            } else {
                $all_cheques_in_queue_list .= 'value="' . $aciq->id . '">Cheque No: ' . $aciq->cheque_no;
            }

            $all_cheques_in_queue_list .=  '</option>';
        }

        return $all_cheques_in_queue_list;
    }

    public function remove_cheque_from_queue(Request $request)
    {
        $id = $request->id;

        $delete = DB::table('cheque_queue')->where('id', $id)->delete();

        if ($delete == 1) {
            return 'success';
        } else {
            return 'error';
        }
    }

    public function save_cheques_in_queue()
    {
        $cheques_in_queue = DB::table('cheque_queue')->where('is_saved', '0')->get();
        $count = DB::table('cheque_queue')->where('is_saved', '0')->count();

        foreach ($cheques_in_queue as $ciq) {
            if (
                $ciq->amount == 0 ||
                $ciq->amount == null ||
                $ciq->vendor_id == 0 ||
                $ciq->vendor_id == null ||
                $ciq->payee_id == 0 ||
                $ciq->payee_id == null ||
                $ciq->date == null ||
                $ciq->received_date == null ||
                $ciq->cheque_no == '' ||
                strpos($ciq->cheque_no, '@') !== false ||
                $ciq->account_no == '' ||
                strpos($ciq->account_no, '@') !== false ||
                $ciq->bank_code == '' ||
                strpos($ciq->bank_code, '@') !== false ||
                $ciq->bank_brnch_code == '' ||
                strpos($ciq->bank_brnch_code, '@') !== false
            ) {
                if (
                    $ciq->cheque_no == '' ||
                    strpos($ciq->cheque_no, '@') !== false
                ) {
                    return json_encode(array('type' => 'error', 'text' => 'undefined cheque/s found'));
                }
                return json_encode(array('type' => 'error', 'text' => 'empty fields in cheque no. ' . $ciq->cheque_no));
            }
        }


        foreach ($cheques_in_queue as $ciq) {

            $bank_id = DB::table('banks')->where('code', $ciq->bank_code)->first()->id;

            $branch_id = DB::table('bank_branches')->where('bank_id', $bank_id)->where('bb_code', $ciq->bank_brnch_code)->first()->id;

            DB::table('cheques')->insert([
                'status_id' => '3',
                'vendor_id' => $ciq->vendor_id,
                'cheque_no' => $ciq->cheque_no,
                'account_no' => $ciq->account_no,
                'bank_id' => $bank_id,
                'bank_branch_id' => $branch_id,
                'deposit_date' => $ciq->date,
                'received_date' => $ciq->received_date,
                'image_front' => $ciq->front_img,
                'image_back' => $ciq->back_img,
                'payee_id' => $ciq->payee_id,
                'amount' => $ciq->amount
            ]);
        }

        DB::table('cheque_queue')->update([
            'is_saved' => '1'

        ]);

        return json_encode(array('type' => 'success', 'text' => $count . ' cheques added successfully'));
    }

    public function get_values_for_setting_queue_default_type(Request $request)
    {
        if ($request->type == 'vendor') {
            $vendors = Vendor::where('is_deleted', '0')->get();
            $list = '';

            foreach ($vendors as $vendor) {
                $list .= '<option value="' . $vendor->id . '">' . $vendor->name . '</option>';
            }
        } else if ($request->type == 'payee') {

            $branches = DB::table('branches')->where('status', '1')->get();

            $list = '';

            foreach ($branches as $branch) {
                $list .= '<option value="' . $branch->id . '">' . $branch->brnch_name . '</option>';
            }
        }

        return $list;
    }

    public function set_queue_default(Request $request)
    {
        $type = $request->type;
        $value = $request->value;

        DB::table('cheque_queue')
            ->where('is_saved', '0')
            ->update([
                $type . '_id' => $value
            ]);

        return response()->json([
            'type' => 'success',
            'text' => $type . ' set successfully'
        ]);
    }
}
