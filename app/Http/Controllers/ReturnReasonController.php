<?php

namespace App\Http\Controllers;

use App\ReturnReason;
use Illuminate\Http\Request;

use DataTables;

class ReturnReasonController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Return Reasons';
        $return_reasons = ReturnReason::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();

        if ($request->ajax()) {
            $return_reasons = ReturnReason::where('is_deleted', '0')->get();
            return DataTables::of($return_reasons)
                ->addColumn('action', function ($return_reasons) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $return_reasons->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $return_reasons->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('return_reasons.index', compact('pageTitle', 'return_reasons', 'sys_date'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        if ($request->title == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Title cannot be empty'));
            die($output);
        }

        if ($request->code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Code cannot be empty'));
            die($output);
        }

        $returnReason               = new ReturnReason;

        $returnReason->title         = $request->title;
        $returnReason->code         = $request->code;

        // $bank->save();
        if ($returnReason->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Return Reason added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function show(ReturnReason $returnReason)
    {
        //
    }


    public function edit(ReturnReason $returnReason)
    {
        return $returnReason;
    }


    public function update(Request $request, ReturnReason $returnReason)
    {

        if ($request->title == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Title cannot be empty'));
            die($output);
        }

        if ($request->code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Code cannot be empty'));
            die($output);
        }

        $returnReason->title         = $request->title;
        $returnReason->code         = $request->code;

        // $bank->save();
        if ($returnReason->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Return Reason updated successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function destroy(ReturnReason $returnReason)
    {

        $returnReason->is_deleted = '1';

        if ($returnReason->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Return Reason removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }
}
