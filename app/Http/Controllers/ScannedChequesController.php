<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DataTables;

class ScannedChequesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Scanned Cheques';
        $sys_date = Controller::get_system_date();
        $date = Carbon::now()->setTimezone('Asia/Colombo')->toDateString();
        $cheques = DB::table('cheque_queue')->where('is_saved', '1')->where('scanned_date', $date)->get();

        if ($request->ajax()) {

            $date = $request->date;
            $cheques = DB::table('cheque_queue')->where('is_saved', '1')->where('scanned_date', $date)->get();
            // dd($cheques);
            return DataTables::of($cheques)
                ->addColumn('vendor_id', function ($cheque) {
                    $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

                    return $vendor->name;
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('bank_branch', function ($cheque) {
                    $bank_branch =  DB::table('banks as b')
                        ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
                        ->select('b.name', 'bb.bb_name')
                        ->where('b.code', $cheque->bank_code)
                        ->where('bb.bb_code', $cheque->bank_brnch_code)
                        ->first();

                    return $bank_branch->name . ' (' . $bank_branch->bb_name . ')';
                })
                ->addColumn('res', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('action', function ($cheque) {

                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="show_cheque(' . $cheque->id . ')">Show</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make('true');
        }

        return view('cheques.scanned', compact('pageTitle', 'sys_date'));
    }

    public function show_cheque_image(Request $request)
    {
        $id = $request->id;

        $cheque = DB::table('cheque_queue')->where('id', $id)->first();

        $image_front = explode("\\", $cheque->front_img);

        $image_behind = explode("\\", $cheque->back_img);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        $cheque_no = $cheque->cheque_no;

        return json_encode(array('type' => 'success', 'image_front' => $image_front, 'image_behind' => $image_behind, 'cheque_no' => $cheque_no));
    }
}
