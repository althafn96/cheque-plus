<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class ChequesToVendorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Cheques to Vendor';
        $sys_date = Controller::get_system_date();
        $cheques = DB::table('cheques')->get();

        if ($request->ajax()) {
            $cheques = DB::table('cheques')
                ->join('cheques_to_vendor', 'cheques.id', '=', 'cheques_to_vendor.cheque_id')
                ->join('vendors', 'cheques_to_vendor.vendor_id', '=', 'vendors.id')
                ->select('cheques.*', 'cheques_to_vendor.granted_date', 'vendors.code as vendor_code', 'vendors.name as vendor_name')
                ->where('cheques.status_id', '2')
                ->where('cheques.is_deleted', '0')
                ->where('cheques.is_hold', '0')
                ->get();

            return DataTables::of($cheques)
                ->addColumn('vendor_id', function ($cheque) {
                    return $cheque->vendor_name . ' (' . $cheque->vendor_code . ')';
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('bank_branch', function ($cheque) {
                    $bank_branch =  DB::table('banks as b')
                        ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
                        ->select('b.name', 'bb.bb_name')
                        ->where('b.id', $cheque->bank_id)
                        ->where('bb.id', $cheque->bank_branch_id)
                        ->first();

                    return $bank_branch->name . ' (' . $bank_branch->bb_name . ')';
                })
                ->addColumn('res', function ($cheque) {
                    $res =  '';

                    return $res;
                })
                ->addColumn('grant_date', function ($cheque) {
                    $grant_date =  $cheque->granted_date;

                    return $grant_date;
                })
                ->addColumn('action', function ($cheque) {

                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="show_cheque(' . $cheque->id . ')">Show</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('cheques.vendor', compact('pageTitle', 'sys_date'));
    }
}
