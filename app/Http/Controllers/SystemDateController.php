<?php

namespace App\Http\Controllers;

use App\SystemDate;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class SystemDateController extends Controller
{

    public function update(Request $request)
    {
        $old_date = $request->sys_date;

        $date = new DateTime($old_date);
        $date->modify('+1 day');
        $new_date = $date->format('Y-m-d');

        $holiday_events = DB::table('holiday_calendar')->get();

        foreach ($holiday_events as $event) {
            if ($new_date >= $event->start &&  $new_date < $event->end) {
                $new_date = $event->end;
            }
        }

        $sys_date = SystemDate::find(1);

        $sys_date->sys_date = $new_date;
        $sys_date->save();

        if ($sys_date->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'System Date updated successfully', 'sys_date' => $new_date));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }

    public function undo(Request $request)
    {
        $old_date = $request->sys_date;

        $date = new DateTime($old_date);
        $date->modify('-1 day');
        $new_date = $date->format('Y-m-d');

        $holiday_events = DB::table('holiday_calendar')->orderBy('id', 'desc')->get();

        foreach ($holiday_events as $event) {
            if ($new_date >= $event->start &&  $new_date < $event->end) {
                $new_date = $event->start;
            }
        }

        $sys_date = SystemDate::find(1);

        $sys_date->sys_date = $new_date;
        $sys_date->save();

        if ($sys_date->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'System Date updated successfully', 'sys_date' => $new_date));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }
}
