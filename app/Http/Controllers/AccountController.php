<?php

namespace App\Http\Controllers;

use App\Account;
use App\Bank;
use App\BankBranch;
use App\Branch;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Accounts';
        $accounts = Account::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();


        if ($request->ajax()) {
            $accounts = Account::where('is_deleted', '0')->get();
            return DataTables::of($accounts)
                ->addColumn('bank_id', function ($accounts) {
                    $bank_id =  DB::table('accounts')->where('id', $accounts->id)->first()->bank_id;
                    $bank = DB::table('banks')->where('id', $bank_id)->first()->name;

                    return $bank;
                })
                ->addColumn('bank_branch_id', function ($accounts) {
                    $bank_branch_id =  DB::table('accounts')->where('id', $accounts->id)->first()->bank_branch_id;
                    $bank_branch = DB::table('bank_branches')->where('id', $bank_branch_id)->first();

                    $bank_branch = $bank_branch->bb_name . ' (' . $bank_branch->bb_code . ')';

                    return $bank_branch;
                })
                ->addColumn('action', function ($accounts) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $accounts->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $accounts->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('accounts.index', compact('pageTitle', 'accounts', 'sys_date'));
    }


    public function create()
    {
    }


    public function store(Request $request)
    {
        if ($request->account_no == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Account No. cannot be empty'));
            die($output);
        }
        if ($request->bank_id == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank cannot be empty'));
            die($output);
        }
        if ($request->bank_branch_id == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank Branch cannot be empty'));
            die($output);
        }

        $account               = new Account;

        $account->account_title      = $request->account_title;
        $account->account_no      = $request->account_no;
        $account->bank_id      = $request->bank_id;
        $account->bank_branch_id = $request->bank_branch_id;
        $account->account_name = $request->account_name;
        $account->bank_endorsement = $request->bank_endorsement;
        $account->company_branch = '';

        // $bank->save();
        if ($account->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Account added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function show(Account $account)
    {
    }


    public function edit(Account $account)
    {
        $bank_id = $account->bank_id;
        $bank_branch_id = $account->bank_branch_id;
        $company_branch = $account->company_branch;

        $banks = Bank::where('status', '1')->get();
        $banks_select = '';

        foreach ($banks as $bank) {
            if ($bank->id == $bank_id) {
                $banks_select .= '<option selected value="' . $bank->id . '">' . $bank->name . '</option>';
            } else {
                $banks_select .= '<option value="' . $bank->id . '">' . $bank->name . '</option>';
            }
        }

        $bank_branches = BankBranch::where('status', '1')->get();
        $bank_branches_select = '';

        foreach ($bank_branches as $bank_brnch) {
            if ($bank_brnch->id == $bank_branch_id) {
                $bank_branches_select .= '<option selected value="' . $bank_brnch->id . '">' . $bank_brnch->bb_name . ' (' . $bank_brnch->bb_code . ')' . '</option>';
            } else {
                $bank_branches_select .= '<option value="' . $bank_brnch->id . '">' . $bank_brnch->bb_name . ' (' . $bank_brnch->bb_code . ')' . '</option>';
            }
        }

        $company_branches = Branch::where('status', '1')->get();
        $company_branches_select = '';

        foreach ($company_branches as $branch) {
            if ($branch->id == $company_branch) {
                $company_branches_select .= '<option selected value="' . $branch->id . '">' . $branch->brnch_name . '</option>';
            } else {
                $company_branches_select .= '<option value="' . $branch->id . '">' . $branch->brnch_name . '</option>';
            }
        }

        $output = json_encode(array('account' => $account, 'banks' => $banks_select, 'bank_branches' => $bank_branches_select, 'company_branches' => $company_branches_select));
        die($output);
    }


    public function update(Request $request, Account $account)
    {
        if ($request->account_no == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Account No. cannot be empty'));
            die($output);
        }
        if ($request->bank_id == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank cannot be empty'));
            die($output);
        }
        if ($request->bank_branch_id == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank Branch cannot be empty'));
            die($output);
        }

        $account->account_title      = $request->account_title;
        $account->account_no      = $request->account_no;
        $account->bank_id      = $request->bank_id;
        $account->bank_branch_id = $request->bank_branch_id;
        $account->account_name = $request->account_name;
        $account->bank_endorsement = $request->bank_endorsement;
        $account->company_branch = '';
        $account->status = $request->status;

        // $bank->save();
        if ($account->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Account updated successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }

    public function destroy(Account $account)
    {

        $account->is_deleted = '1';
        $account->status     = '0';

        if ($account->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Account removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }

    public function fetch_bank_branches(Request $request)
    {

        if (isset($_GET['search'])) {

            if (isset($_GET['bank'])) {
                $param = $_GET['search'];
                $bank = $_GET['bank'];
                $bank_branches = BankBranch::where('status', '1')->where('bank_id', $bank)->where('name', 'like', '%' . $param . '%')->get()->toArray();
            } else {
                $param = $_GET['search'];
                $bank_branches = BankBranch::where('status', '1')->where('name', 'like', '%' . $param . '%')->get()->toArray();
            }
        } else {

            if (isset($_GET['bank'])) {
                $bank = $_GET['bank'];
                $bank_branches = BankBranch::where('status', '1')->where('bank_id', $bank)->get()->toArray();
            } else {
                $bank_branches = BankBranch::where('status', '1')->get()->toArray();
            }
        }
        return $bank_branches;
    }
}
