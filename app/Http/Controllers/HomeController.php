<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $pageTitle = 'Home';
        $sys_date = Controller::get_system_date();
        return view('home', compact('pageTitle', 'sys_date'));
    }

    public function open_scanner()
    {
        shell_exec("C://Program Files (x86)/Canon Electronics/Scanning Utility CR150/Su150.exe");
    }
}
