<?php

namespace App\Http\Controllers;

use App\BankBranch;
use App\Bank;
use Illuminate\Http\Request;
use DataTables;

class BankBranchesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Bank Branches';
        $bank_branches = BankBranch::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();

        if ($request->ajax()) {
            $bank_branches = BankBranch::latest()->where('is_deleted', '0')->get();
            return DataTables::of($bank_branches)
                ->addColumn('bank', function ($bank_branches) {
                    $bank =  BankBranch::find($bank_branches->id)->bank->name;

                    return $bank;
                })
                ->addColumn('action', function ($bank_branches) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $bank_branches->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $bank_branches->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('bank_branches.index', compact('pageTitle', 'bank_branches', 'sys_date'));
    }

    public function fetch_banks()
    {

        if (isset($_GET['search'])) {

            $param = $_GET['search'];
            $banks = Bank::where('status', '1')->where('name', 'like', '%' . $param . '%')->get()->toArray();
        } else {

            $banks = Bank::where('status', '1')->get()->toArray();
        }
        return $banks;
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        if ($request->bb_code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch code cannot be empty'));
            die($output);
        }

        if ($request->bb_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch name cannot be empty'));
            die($output);
        }

        if ($request->bb_bank == '0') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank cannot be empty'));
            die($output);
        }

        if ($request->bb_address_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch address cannot be empty (atleast 1 field needs to be filled)'));
            die($output);
        }

        if ($request->bb_tel_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch telephone number cannot be empty'));
            die($output);
        }

        if ($request->bb_email_1 != '' && !filter_var($request->bb_email_1, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        if ($request->bb_email_2 != '' && !filter_var($request->bb_email_2, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        $branch               = new BankBranch;

        $branch->bb_code      = $request->bb_code;
        $branch->bb_name      = $request->bb_name;
        $branch->bank_id      = $request->bb_bank;
        $branch->bb_address_1 = $request->bb_address_1;
        $branch->bb_address_2 = $request->bb_address_2;
        $branch->bb_address_3 = $request->bb_address_3;
        $branch->bb_tel_1     = $request->bb_tel_1;
        $branch->bb_tel_2     = $request->bb_tel_2;
        $branch->bb_tel_3     = $request->bb_tel_3;
        $branch->bb_fax_1     = $request->bb_fax_1;
        $branch->bb_fax_2     = $request->bb_fax_2;
        $branch->bb_email_1   = $request->bb_email_1;
        $branch->bb_email_2   = $request->bb_email_2;

        // $bank->save();
        if ($branch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank branch added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function show(BankBranch $bankBranch)
    {
        //
    }


    public function edit(BankBranch $bankBranch)
    {
        $bank_id = $bankBranch->bank->id;

        $banks = Bank::where('status', '1')->get();
        $banks_select = '';

        foreach ($banks as $bank) {
            if ($bank->id == $bank_id) {
                $banks_select .= '<option selected value="' . $bank->id . '">' . $bank->name . '</option>';
            } else {
                $banks_select .= '<option value="' . $bank->id . '">' . $bank->name . '</option>';
            }
        }

        $output = json_encode(array('bankBranch' => $bankBranch, 'banks' => $banks_select));
        die($output);
    }


    public function update(Request $request, BankBranch $bankBranch)
    {
        if ($request->bb_code == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch code cannot be empty'));
            die($output);
        }

        if ($request->bb_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch name cannot be empty'));
            die($output);
        }

        if ($request->bb_bank == '0') {
            $output = json_encode(array('type' => 'error', 'text' => 'Bank cannot be empty'));
            die($output);
        }

        if ($request->bb_address_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch address cannot be empty (atleast 1 field needs to be filled)'));
            die($output);
        }

        if ($request->bb_tel_1 == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch telephone number cannot be empty'));
            die($output);
        }

        if ($request->bb_email_1 != '' && !filter_var($request->bb_email_1, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        if ($request->bb_email_2 != '' && !filter_var($request->bb_email_2, FILTER_VALIDATE_EMAIL)) {
            $output = json_encode(array('type' => 'error', 'text' => 'Invalid email address'));
            die($output);
        }

        $bankBranch->bb_code      = $request->bb_code;
        $bankBranch->bb_name      = $request->bb_name;
        $bankBranch->bank_id      = $request->bank_id;
        $bankBranch->bb_address_1 = $request->bb_address_1;
        $bankBranch->bb_address_2 = $request->bb_address_2;
        $bankBranch->bb_address_3 = $request->bb_address_3;
        $bankBranch->bb_tel_1     = $request->bb_tel_1;
        $bankBranch->bb_tel_2     = $request->bb_tel_2;
        $bankBranch->bb_tel_3     = $request->bb_tel_3;
        $bankBranch->bb_fax_1     = $request->bb_fax_1;
        $bankBranch->bb_fax_2     = $request->bb_fax_2;
        $bankBranch->bb_email_1   = $request->bb_email_1;
        $bankBranch->bb_email_2   = $request->bb_email_2;
        $bankBranch->status       = $request->status;

        // $bank->save();
        if ($bankBranch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank branch updatd successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function destroy(BankBranch $bankBranch)
    {

        $bankBranch->is_deleted = '1';
        $bankBranch->status     = '0';

        if ($bankBranch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Bank branch removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }
}
