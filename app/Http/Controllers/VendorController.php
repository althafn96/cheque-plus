<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;

use DataTables;

class VendorController extends Controller
{

    public function index(Request $request)
    {

        $pageTitle = 'Vendors';
        $vendors = Vendor::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();

        if ($request->ajax()) {
            $vendors = Vendor::where('is_deleted', '0')->get();
            return DataTables::of($vendors)
                ->addColumn('action', function ($vendors) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $vendors->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $vendors->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('vendors.index', compact('pageTitle', 'vendors', 'sys_date'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        if ($request->name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Vendor Name cannot be empty'));
            die($output);
        }

        $vendor                         = new Vendor;

        $vendor->code                   = $request->code;
        $vendor->name                   = $request->name;
        $vendor->other_name             = $request->other_name;
        $vendor->address                = $request->address;
        $vendor->primary_contact_no     = $request->primary_contact_no;
        $vendor->secondary_contact_no   = $request->secondary_contact_no;
        $vendor->email                  = $request->email;
        $vendor->fax                    = $request->fax;

        // $vendor->save();
        if ($vendor->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Vendor added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function show(Vendor $vendor)
    {
        //
    }


    public function edit(Vendor $vendor)
    {
        return $vendor;
    }


    public function update(Request $request, Vendor $vendor)
    {

        if ($request->name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Vendor Name cannot be empty'));
            die($output);
        }

        $vendor->code                   = $request->code;
        $vendor->name                   = $request->name;
        $vendor->other_name             = $request->other_name;
        $vendor->address                = $request->address;
        $vendor->primary_contact_no     = $request->primary_contact_no;
        $vendor->secondary_contact_no   = $request->secondary_contact_no;
        $vendor->email                  = $request->email;
        $vendor->fax                    = $request->fax;

        // $vendor->save();
        if ($vendor->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Vendor updated successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function destroy(Vendor $vendor)
    {

        $vendor->is_deleted = '1';

        if ($vendor->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Vendor removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }
}
