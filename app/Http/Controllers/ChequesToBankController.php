<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class ChequesToBankController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Cheques to Bank';
        $sys_date = Controller::get_system_date();
        $cheques = DB::table('cheques')->get();

        if ($request->ajax()) {
            $cheques = DB::table('cheques')
                ->join('cheques_to_bank', 'cheques.id', '=', 'cheques_to_bank.cheque_id')
                ->join('accounts', 'cheques_to_bank.account_id', '=', 'accounts.id')
                ->select('cheques.*', 'cheques_to_bank.granted_date', 'accounts.account_no as granted_to')
                ->where('cheques.status_id', '1')
                ->where('cheques.is_deleted', '0')
                ->where('cheques.is_hold', '0')
                ->where('cheques_to_bank.is_returned', '0')
                ->get();

            return DataTables::of($cheques)
                ->addColumn('status', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('vendor_id', function ($cheque) {
                    $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

                    return $vendor->name;
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('bank_branch', function ($cheque) {
                    $bank_branch =  DB::table('banks as b')
                        ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
                        ->select('b.name', 'bb.bb_name')
                        ->where('b.id', $cheque->bank_id)
                        ->where('bb.id', $cheque->bank_branch_id)
                        ->first();

                    return $bank_branch->name . ' (' . $bank_branch->bb_name . ')';
                })
                ->addColumn('res', function ($cheque) {
                    $res =  '';

                    return $res;
                })
                ->addColumn('grant_date', function ($cheque) {
                    $grant_date =  $cheque->granted_date;

                    return $grant_date;
                })
                ->addColumn('granted_to', function ($cheque) {
                    $granted_to =  $cheque->granted_to;

                    return $granted_to;
                })
                ->addColumn('action', function ($cheque) {

                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="show_cheque(' . $cheque->id . ')">Show</button>
                                        <button class="dropdown-item" onclick="mark_return(' . $cheque->id . ')">Mark Return</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make('true');
        }

        return view('cheques.bank', compact('pageTitle', 'sys_date'));
    }

    public function return_cheques(Request $request)
    {
        $pageTitle = 'Cheques to Bank';
        $sys_date = Controller::get_system_date();
        $cheques = DB::table('cheques')->get();

        if ($request->ajax()) {
            $cheques = DB::table('return_cheques')
                ->join('cheques', 'return_cheques.chq_id', '=', 'cheques.id')
                ->join('cheques_to_bank', 'cheques.id', '=', 'cheques_to_bank.cheque_id')
                ->join('accounts', 'cheques_to_bank.account_id', '=', 'accounts.id')
                ->join('return_reasons', 'return_cheques.rr_id', '=', 'return_reasons.id')
                ->select('return_cheques.returned_date', 'cheques.*', 'cheques_to_bank.granted_date', 'accounts.account_no as granted_to', 'return_reasons.title as rr_title')
                ->where('cheques.status_id', '1')
                ->where('cheques.is_deleted', '0')
                ->where('cheques.is_hold', '0')
                ->get();

            return DataTables::of($cheques)
                ->addColumn('status', function ($cheque) {
                    $checkbox =  '';

                    return $checkbox;
                })
                ->addColumn('vendor_id', function ($cheque) {
                    $vendor =  DB::table('vendors')->select('name')->where('id', $cheque->vendor_id)->first();

                    return $vendor->name;
                })
                ->addColumn('payee_id', function ($cheque) {
                    $branch =  DB::table('branches')->select('brnch_name')->where('id', $cheque->payee_id)->first();

                    return $branch->brnch_name;
                })
                ->addColumn('bank_branch', function ($cheque) {
                    $bank_branch =  DB::table('banks as b')
                        ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
                        ->select('b.name', 'bb.bb_name')
                        ->where('b.id', $cheque->bank_id)
                        ->where('bb.id', $cheque->bank_branch_id)
                        ->first();

                    return $bank_branch->name . ' (' . $bank_branch->bb_name . ')';
                })
                ->addColumn('res', function ($cheque) {
                    $res =  '';

                    return $res;
                })
                ->addColumn('grant_date', function ($cheque) {
                    $grant_date =  $cheque->granted_date;

                    return $grant_date;
                })
                ->addColumn('granted_to', function ($cheque) {
                    $granted_to =  $cheque->granted_to;

                    return $granted_to;
                })
                ->rawColumns(['status'])
                ->make('true');
        }

        return view('cheques.bank', compact('pageTitle', 'sys_date'));
    }

    public function mark_return(Request $request)
    {
        $sys_date = Controller::get_system_date();

        if ($request->selected_ids == '') {
            return 'Please select a cheque/cheques to grant to bank';
        }
        $ids_arr = explode(',', $request->selected_ids);
        $cheques_arr = array();

        foreach ($ids_arr as $id) {
            if ($id != '') {
                $cheque_row = DB::table('cheques')->where('id', $id)->first();

                array_push($cheques_arr, $cheque_row);
            }
        }
        $bnk_id = $cheques_arr[0]->bank_id;

        $ciq_select_list = '';
        $total_in_queue = 0;

        $total_amount = 0;

        foreach ($cheques_arr as $ca) {
            if ($ca->id == $cheques_arr[0]->id) {
                $ciq_select_list .= '<option selected value="' . $ca->id . '">Cheque No. ' . $ca->cheque_no . '</option>';
            } else {
                $ciq_select_list .= '<option value="' . $ca->id . '">Cheque No. ' . $ca->cheque_no . '</option>';
            }

            $total_in_queue++;
            $total_amount += $ca->amount;
        }

        $image_front = explode("\\", $cheques_arr[0]->image_front);

        $image_behind = explode("\\", $cheques_arr[0]->image_back);

        $image_front = $image_front[count($image_front) - 1];
        $image_behind = $image_behind[count($image_behind) - 1];

        $bank_branch =  DB::table('banks as b')
            ->join('bank_branches as bb', 'b.id', '=', 'bb.bank_id')
            ->select('b.name', 'bb.bb_name')
            ->where('b.id', $cheques_arr[0]->bank_id)
            ->where('bb.id', $cheques_arr[0]->bank_branch_id)
            ->first();

        $vendor =  DB::table('vendors')->select('name')->where('id', $cheques_arr[0]->vendor_id)->first();

        $return_reasons = DB::table('return_reasons')->where('is_deleted', '0')->get();
        $return_reasons_list = '';
        foreach ($return_reasons as $rr) {
            $return_reasons_list .= '<option value="' . $rr->id . '">' . $rr->title  . '</option>';
        }


        $output = '
                    <div class="row p-1">
                        <div class="col-md-6">
                            <fieldset class="form-group form-group-style">
                                <label for="cheque_no_on_return_queue">Cheque No.</label>
                                <select id="cheque_no_on_return_queue" class="form-control select2">
                                    ' . $ciq_select_list . '
                                </select>
                            </fieldset>
                            <ul class="list-group rc_queue" id="chq_list_details">
                                <li class="list-group-item"><strong>Branch:</strong> ' . $bank_branch->name . ' (' . $bank_branch->bb_name . ')' . ' </li>
                                <li class="list-group-item"><strong>Vendor:  </strong>' . $vendor->name . '</li>
                                <li class="list-group-item"><strong>Deposit Date:  </strong>' . $cheques_arr[0]->deposit_date . '</li>
                                <li class="list-group-item"><strong>Account No.:  </strong>' . $cheques_arr[0]->account_no . '</li>
                                <li class="list-group-item"><strong>Amount:  </strong> Rs. ' . $cheques_arr[0]->amount . '</li>
                            </ul>

                            <ul class="mt-2 list-unstyled">
                                <li>Total Number of Cheques</li>
                                <li class="lead text-bold-800">' . $total_in_queue . '</li>
                            </ul>
                            <p>AMOUNT</p>
                            <h2 class="pb-1">Rs. ' . $total_amount . '</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="form-group form-group-style">
                                        <label for="return_date">Return Date <span class="red">*</span></label>
                                        <input value="' . $sys_date . '" type="date" class="form-control" id="return_date">
                                    </fieldset> 
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group form-group-style">
                                        <label for="return_reason">Return Reason <span class="red">*</span></label>
                                        <select id="return_reason" class="form-control select2">
                                            <option value="0">--Select Return Reason--</option>
                                            ' . $return_reasons_list . '
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <button dataList="' . $request->selected_ids . '" id="submit_return_cheques" type="button" class="btn btn-info btn-glow float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 rc_queue" id="chq_images">
                            <img src="' . asset('chq_source/' . $image_front) . '" style="width:100%;height:auto;margin-bottom:10px;">
                            <img src="' . asset('chq_source/' . $image_behind) . '" style="width:100%;height:auto;margin-bottom:10px;">
                        </div>
                    </div>

                    <script> $(".select2").select2(); </script>
                ';

        return $output;
    }

    public function submit_return_cheques(Request $request)
    {
        $selected_ids = $request->selected_ids;
        $return_date = $request->return_date;
        $return_reason = $request->return_reason;

        if ($return_date == '') {
            return json_encode(array('type' => 'error', 'text' => 'Please enter the return date'));
        }
        if ($return_reason == '0') {
            return json_encode(array('type' => 'error', 'text' => 'Please select a return reason'));
        }

        $ids_arr = explode(',', $selected_ids);

        foreach ($ids_arr as $id) {
            if ($id != '') {
                DB::table('cheques_to_bank')->where('cheque_id', $id)->update(['is_returned' => '1']);

                DB::table('return_cheques')->insert([
                    'chq_id' => $id,
                    'rr_id' => $return_reason,
                    'returned_date' => $return_date,
                    'created_at' => Carbon::now()->setTimezone('Asia/Colombo')
                ]);
            }
        }

        return json_encode(array('type' => 'success', 'text' => 'return cheques saved successfully'));
    }
}
