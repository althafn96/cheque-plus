<?php

namespace App\Http\Controllers;

use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PDF;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pageTitle = 'Reports';
        $sys_date = Controller::get_system_date();

        return view('reports.index', compact('pageTitle', 'sys_date'));
    }

    public function validate_input(Request $request)
    {
        $sys_date = Controller::get_system_date();

        if ($request->type == '0') {
            return response()->json([
                'type' => 'error',
                'text' => 'report type cant be empty'
            ]);
        }
        if ($request->from_date == null || $request->from_date == '') {
            return response()->json([
                'type' => 'error',
                'text' => 'from date cant be empty'
            ]);
        }
        if ($request->to_date == null || $request->to_date == '') {
            return response()->json([
                'type' => 'error',
                'text' => 'to date cant be empty'
            ]);
        }

        if ($request->type == '4' || $request->type == '9') {
            if ($request->customer == '0') {
                return response()->json([
                    'type' => 'error',
                    'text' => 'customer cant be empty'
                ]);
            }
        }

        return response()->json([
            'type' => 'success'
        ]);
    }

    public function generate(Request $request)
    {
        $type = $request->type;
        $from = $request->from_date;
        $to = $request->to_date;
        $title = '';

        $report_data = array('from' => $request->from_date, 'to' => $request->to_date);

        if ($type == '1') {
            $type = 'Received_Cheques_' . $from . '_' . $to;
            $title = 'Received Cheque Details';

            $grouping_set = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques.received_date', '>=', $from)
                ->where('cheques.received_date', '<=', $to)
                ->groupBy('branches.id')
                ->get();
            $cheques = DB::table('cheques')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->select('cheques.*', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code')
                ->where('cheques.received_date', '>=', $from)
                ->where('cheques.received_date', '<=', $to)
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '2') {
            $type = 'Cheques_Granted_to_Bank_' . $from . '_' . $to;
            $title = 'Cheques Granted to Bank';

            $grouping_set = DB::table('cheques_to_bank')
                ->join('cheques', 'cheques.id', '=', 'cheques_to_bank.cheque_id')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques_to_bank.granted_date', '>=', $from)
                ->where('cheques_to_bank.granted_date', '<=', $to)
                ->where('cheques.status_id', '=', '1')
                ->groupBy('branches.id')
                ->get();

            $cheques = DB::table('cheques_to_bank')
                ->join('cheques', 'cheques.id', '=', 'cheques_to_bank.cheque_id')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->join('accounts', 'accounts.id', '=', 'cheques_to_bank.account_id')
                ->select('cheques.*', 'cheques_to_bank.granted_date', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code', 'accounts.account_no as our_account_no')
                ->where('cheques_to_bank.granted_date', '>=', $from)
                ->where('cheques_to_bank.granted_date', '<=', $to)
                ->where('cheques.status_id', '=', '1')
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '3') {
            $type = 'Cheques_in_Hand_Details_' . $from . '_' . $to;
            $title = 'Cheques in Hand Details';

            $grouping_set = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->groupBy('branches.id')
                ->get();

            $cheques = DB::table('cheques')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->select('cheques.*', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '4') {
            $type = 'Customer_Individual_Report_' . $from . '_' . $to;
            $title = 'Customer Individual Report';

            $customer = DB::table('vendors')->where('id', $request->customer)->first();
            $report_data['customer'] = $customer->name;

            $grouping_set = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.vendor_id', '=', $request->customer)
                ->groupBy('branches.id')
                ->get();

            $cheques = DB::table('cheques')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('vendors', 'vendors.id', '=', 'cheques.vendor_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->select('cheques.*', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.vendor_id', '=', $request->customer)
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '5') {
            $type = 'Cheques_on_Hold_' . $from . '_' . $to;
            $title = 'Cheques on Hold';

            $grouping_set = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.is_hold', '=', '1')
                ->groupBy('branches.id')
                ->get();

            $cheques = DB::table('cheques')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->select('cheques.*', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.is_hold', '=', '1')
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '6') {
            $type = 'Cheques_in_Hand_Details_CustomerWise__' . $from . '_' . $to;
            $title = 'Cheques in Hand Details ( Customer Wise )';

            $grouping_set = DB::table('cheques')
                ->join('vendors', 'vendors.id', '=', 'cheques.vendor_id')
                ->select('vendors.id as vendor_id', 'vendors.name as vendor_name', 'vendors.code as vendor_code')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->groupBy('vendors.id')
                ->get();

            $cheques = DB::table('cheques')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->join('vendors', 'vendors.id', '=', 'cheques.vendor_id')
                ->select('cheques.*', 'banks.name as bank_name', 'banks.code as bank_code', 'bank_branches.bb_name as bb_name', 'bank_branches.bb_code as bb_code')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->orderBy('id', 'desc')
                ->get();
        } else if ($type == '7') {
            $type = 'Cheques_in_Hand_Summary_PayeeWise__' . $from . '_' . $to;
            $title = 'Cheques in Hand Summary ( Payee Wise )';

            $grouping_set = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select('branches.id as payee_id', 'branches.brnch_name as payee_name')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->groupBy('branches.id')
                ->get();

            $cheques = DB::table('cheques')
                ->join('branches', 'branches.id', '=', 'cheques.payee_id')
                ->select(DB::raw('count(*) as count'), DB::raw('sum(cheques.amount) as sum'), 'cheques.payee_id as payee_id', 'cheques.deposit_date as deposit_date')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->groupBy('deposit_date')
                ->groupBy('cheques.payee_id')
                ->orderBy('cheques.id', 'desc')
                ->get();
        } else if ($type == '8') {
            $type = 'Cheques_in_Hand_Summary_CustomerWise__' . $from . '_' . $to;
            $title = 'Cheques in Hand Summary ( Customer Wise )';

            $grouping_set = '';

            $cheques = DB::table('cheques')
                ->join('vendors', 'vendors.id', '=', 'cheques.vendor_id')
                ->select(DB::raw('count(*) as count'), DB::raw('sum(cheques.amount) as sum_value'),  'vendors.code as vendor_code', 'vendors.name as vendor_name')
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '3')
                ->groupBy('vendors.id')
                ->orderBy('cheques.id', 'desc')
                ->get();
        } else if ($type == '9') {

            $customer = DB::table('vendors')->where('id', $request->customer)->first();
            $report_data['customer'] = $customer->name;

            $grouping_set = '';

            $type = 'Cheques_Assigned_to_' . $customer->name . '_' . $from . '_' . $to;
            $title = 'Cheques Assigned';

            $cheques = DB::table('cheques_to_vendor')
                ->join('cheques', 'cheques.id', '=', 'cheques_to_vendor.cheque_id')
                ->join('banks', 'banks.id', '=', 'cheques.bank_id')
                ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
                ->select(
                    'cheques.*',
                    'banks.name as bank_name',
                    'banks.code as bank_code',
                    'bank_branches.bb_name as bb_name',
                    'bank_branches.bb_code as bb_code',
                    'cheques_to_vendor.granted_date as granted_date'
                )
                ->where('cheques.deposit_date', '>=', $from)
                ->where('cheques.deposit_date', '<=', $to)
                ->where('cheques.status_id', '=', '2')
                ->where('cheques.vendor_id', '=', $request->customer)
                ->orderBy('cheques.id', 'desc')
                ->get();
        } else if ($type == '10') {

            $type = 'Cheque_Deposit_Details_to_' . $customer->name . '_' . $from . '_' . $to;
            $title = 'Cheque Deposit Details';

            // $grouping_set = DB::table('cheques_to_bank')
            //     ->join('cheques', 'cheques.id', '=', 'cheques_to_bank.cheque_id')
            //     ->join('accounts', 'accounts.id', '=', 'cheques.accounts_id')
            //     ->join('banks', 'banks.id', '=', 'cheques.bank_id')
            //     ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
            //     ->select('accounts.id as account_id', 'cheques_to_bank.grant_date as payee_name')
            //     ->where('cheques.deposit_date', '>=', $from)
            //     ->where('cheques.deposit_date', '<=', $to)
            //     ->where('cheques.status_id', '=', '3')
            //     ->groupBy('accounts.id')
            //     ->get();

            // $cheques = DB::table('cheques_to_vendor')
            //     ->join('cheques', 'cheques.id', '=', 'cheques_to_vendor.cheque_id')
            //     ->join('banks', 'banks.id', '=', 'cheques.bank_id')
            //     ->join('bank_branches', 'bank_branches.id', '=', 'cheques.bank_branch_id')
            //     ->select(
            //         'cheques.*',
            //         'banks.name as bank_name',
            //         'banks.code as bank_code',
            //         'bank_branches.bb_name as bb_name',
            //         'bank_branches.bb_code as bb_code',
            //         'cheques_to_vendor.granted_date as granted_date'
            //     )
            //     ->where('cheques.deposit_date', '>=', $from)
            //     ->where('cheques.deposit_date', '<=', $to)
            //     ->where('cheques.status_id', '=', '2')
            //     ->where('cheques.vendor_id', '=', $request->customer)
            //     ->orderBy('cheques.id', 'desc')
            //     ->get();

            dd();
        }

        return $this->printpdf($cheques, $grouping_set, $type, $title, $report_data, $request->type);
    }

    public function printpdf($cheques, $grouping_set, $type, $title, $report_data, $r_type)
    {
        $sys_date = Controller::get_system_date();
        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $data = [
            'title' => 'Cheque Plus - ' . $title,
            'cheques' => $cheques,
            'heading' => $title,
            'report_data' => $report_data,
            'sys_date' => $sys_date,
            'r_type' => $r_type,
            'grouping_set' => $grouping_set
        ];

        $pdf->loadView('reports.pdf_view', $data);
        return $pdf->download($type . '.pdf');
    }

    public function get_customer(Request $request)
    {
        if ($request->has('search')) {

            $param = $request->search;

            $customers = DB::table('vendors')
                ->where('name', 'like', '%' . $param . '%')
                ->where('is_deleted', '0')
                ->get();
        } else {

            $customers = DB::table('vendors')->where('is_deleted', '0')->get();
        }

        return $customers;
    }
}
