<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;
use DataTables;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $pageTitle = 'Branches';
        $branches = Branch::where('is_deleted', '0')->get();
        $sys_date = Controller::get_system_date();

        if ($request->ajax()) {
            $branches = Branch::latest()->where('is_deleted', '0')->get();
            return DataTables::of($branches)
                ->addColumn('action', function ($branches) {
                    $button =  '<button type="button" class="btn btn-info btn-min-width dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -141px, 0px);">
                                        <button class="dropdown-item" onclick="edit_record(' . $branches->id . ')">Edit</button>
                                        <button class="dropdown-item" onclick="remove_record(' . $branches->id . ')">Remove</button>
                                    </div>';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('branches.index', compact('pageTitle', 'branches', 'sys_date'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        if ($request->brnch_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch name cannot be empty'));
            die($output);
        }
        if ($request->brnch_contact_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Contact name cannot be empty'));
            die($output);
        }

        $branch               = new Branch;

        $branch->brnch_name         = $request->brnch_name;
        $branch->brnch_location     = $request->brnch_location;
        $branch->brnch_contact_name = $request->brnch_contact_name;
        $branch->brnch_contact_no   = $request->brnch_contact_no;
        $branch->brnch_email        = $request->brnch_email;

        // $bank->save();
        if ($branch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Branch added successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }

    public function show(Branch $branch)
    {
        //
    }


    public function edit(Branch $branch)
    {
        return $branch;
    }


    public function update(Request $request, Branch $branch)
    {
        if ($request->brnch_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Branch name cannot be empty'));
            die($output);
        }
        if ($request->brnch_contact_name == '') {
            $output = json_encode(array('type' => 'error', 'text' => 'Contact name cannot be empty'));
            die($output);
        }

        $branch->brnch_name         = $request->brnch_name;
        $branch->brnch_location     = $request->brnch_location;
        $branch->brnch_contact_name = $request->brnch_contact_name;
        $branch->brnch_contact_no   = $request->brnch_contact_no;
        $branch->brnch_email        = $request->brnch_email;

        // $bank->save();
        if ($branch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Branch updated successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }


    public function destroy(Branch $branch)
    {

        $branch->is_deleted = '1';
        $branch->status     = '0';

        if ($branch->save()) {
            $output = json_encode(array('type' => 'success', 'text' => 'Branch removed successfully'));
            die($output);
        } else {
            $output = json_encode(array('type' => 'error', 'text' => 'Unkown error occurred. Please try again later'));
            die($output);
        }
    }

    public function fetch_company_branches(Request $request)
    {

        if (isset($_GET['search'])) {

            $param = $_GET['search'];
            $banks = Branch::where('status', '1')->where('brnch_name', 'like', '%' . $param . '%')->get()->toArray();
        } else {

            $banks = Branch::where('status', '1')->get()->toArray();
        }
        return $banks;
    }
}
