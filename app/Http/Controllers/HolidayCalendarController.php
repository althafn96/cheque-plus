<?php

namespace App\Http\Controllers;

use App\HolidayCalendar;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect, Response;

class HolidayCalendarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pageTitle = 'Holiday Maintenance';
        $sys_date = Controller::get_system_date();


        return view('holiday-maintenance.index')->with(compact('pageTitle', 'sys_date'));
    }

    public function fetch_holidays()
    {
        $data = DB::table('holiday_calendar')->get();
        return Response::json($data);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $daterange = $request->daterange;
        $daterange_arr = explode(" - ", $daterange);

        $start = $daterange_arr[0];
        $end = $daterange_arr[1];

        $end = new DateTime($end);
        $end->modify('+1 day');

        if ($request->title == null || $request->title == '') {
            $title = 'Untitled Event';
        } else {
            $title = $request->title;
        }

        DB::table('holiday_calendar')->insert([
            'title' => $title,
            'start' => $start,
            'end' => $end,
            'allDay' => 'true'
        ]);

        $output = json_encode(array('type' => 'success', 'text' => 'Holiday Event added successfully'));

        return $output;
    }


    public function show(HolidayCalendar $holidayCalendar)
    {
        //
    }


    public function edit(Request $request, HolidayCalendar $holidayCalendar)
    {
        $holiday_event = DB::table('holiday_calendar')->where('id', $request->id)->first();

        return Response::json($holiday_event);
    }


    public function update(Request $request, HolidayCalendar $holidayCalendar)
    {
        $id = $request->id;
        $daterange = $request->daterange;
        $daterange_arr = explode(" - ", $daterange);

        $start = $daterange_arr[0];
        $end = $daterange_arr[1];

        $end = new DateTime($end);
        $end->modify('+1 day');

        if ($request->title == null || $request->title == '') {
            $title = 'Untitled Event';
        } else {
            $title = $request->title;
        }

        DB::table('holiday_calendar')->where('id', $id)->update([
            'title' => $title,
            'start' => $start,
            'end' => $end,
            'allDay' => 'true'
        ]);

        $output = json_encode(array('type' => 'success', 'text' => 'Holiday Event updated successfully'));

        return $output;
    }


    public function destroy(Request $request, HolidayCalendar $holidayCalendar)
    {
        $id = $request->id;

        DB::table('holiday_calendar')->where('id', $id)->delete();

        $output = json_encode(array('type' => 'success', 'text' => 'Holiday Event removed successfully'));

        return $output;
    }
}
